<?php


class GigyaWrap
{


  private $gigyaSecret;
  private $gigyaApiKey;

  /**
   * construct
   *
   * @params $this->gigyaSecret
   * @params $this->gigyaApiKey
   *
   */
	function __construct( $gSecret = "ffe9+h3adarj4iWDtnhzTSNfVQvnOxRiD4yo8zzQccY=", $gApiKey = "2_AqyN-W_67zLgydvtyFEK_-mk1kkijTsJqjLchVKrgtNkJ-JjcSdAd6gXn7tVEB_6"){
    $this->gigyaSecret = sfConfig::get('app_gigya_secretkey', $gSecret);
    $this->gigyaApiKey = sfConfig::get('app_gigya_apikey', $gApiKey);

  }

  /**
   * writeLog
   *
   * @params gigyaResponse $response
   *
   */
  public function writeLog($response)
  {
    //error_log($response->getLog());  // write the Gigya's response log into the log file

    return null;
  }

	/**
	 * Method: notifySiteLogout
   */
	public function notifySiteLogout($apiKey, $secretKey, $uid)
  {

   $gsRequest = new GSRequest($apiKey, $secretKey, 'socialize.logout');
   $gsRequest->setParam("uid", $uid);
   $gsResponse = $gsRequest->send();

   return ($gsResponse->getErrorCode() == 0);


  }

	/**
	 * Method: notifySiteLogin
   * Use case: User logged-in/registered using the site credentials (not through Gigya Social Login)
	 *
   *
   * Flow:
	 * 1. Notify Gigya of the occurrence (a site user logging in)
	 * Create a session cookie, so as to maintain the client application synchronized with the user state (for Gigya's usage)
   */
	public function notifySiteLogin($uid = "", $isNewUser = false, $userInfoJson = null ){
		$request = new GSRequest($this->gigyaApiKey, $this->gigyaSecret, "socialize.notifyLogin");
		$request->setParam("siteUID",$uid); // set the "siteUID" parameter to user's ID
		if($isNewUser){
			$request->setParam("newUser",$isNewUser); // let gigya server know that it's a new user (not returning user)
		}
		if($userInfoJson != null){
			$request->setParam("userInfo",$userInfoJson);
		}
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return $response->getErrorMessage(); // return the error message
		}

		// Create a session cookie
		try {
			setcookie ($response->getString("cookieName"), $response->getString("cookieValue"),0,$response->getString("cookiePath"),$response->getString("cookieDomain"));
		} catch (Exception $e) {}

		return "success";
	}

	//--------------------------------------------------------------------------------------------------------------
	// Method: finalizeGigyaRegistration
	//	    Notify Gigya of the completion of the registration process.
	//	    The method also replaces Gigya UID in the Gigya's user account with the site UID (that was generated by our local site DB).
	//	    (in other words - links between Gigya user account and site user account)
	//	 	  Note: Please refer to Gigya's Developer'd guide->Social Login->Best Practice. This method implements step 9.
	// Parameters:
	//	    gigyaUID - user ID designated by Gigya
	//	    siteUID - user ID designated by our site, to set in Gigya's user management system (replace the Gigya UID)
	//-------------------------------------------------------------------------------------------------------------
	public function finalizeGigyaRegistration($gigyaUID = "",$siteUID = ""){

		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.notifyRegistration");
		$request->setParam("uid",$gigyaUID); // set the "uid" parameter to the Gigya user's ID
		$request->setParam("siteUID",$siteUID); // set the "siteUID" parameter to user's ID in GCS
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return false;
		}

		return true;
	}



	//------------------------------------------------------------------------------------------------------
	// GCS Related Methods
	//------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// getUserProfileData
	// 	The method retrieves user profile data from GCS.
	//	Parameter:
	//		UID - the user's GCS ID
	//--------------------------------------------------------------------------
	public function getUserProfileData($UID){

		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.getUserData");
		$request->setParam("UID",$UID); // set the "uid" parameter to user's ID
		$request->setParam("fields","*");
		$response = $request->send();
		if($response->getErrorCode()==0){
			$userObject = json_decode($response->getString("data"),true);
			return $userObject;
		}
		else { // Error!
			$this->writeLog($response);
			return null;
		}
	}

	//--------------------------------------------------------------------------
	// setUserProfileData
	// 	The method updates user profile data in GCS.
	//	Parameter:
	//		UID - the user's GCS ID
	//--------------------------------------------------------------------------
	public function setUserProfileData($UID,$userArray,$append=false){

		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.setUserData");
		$request->setParam("UID",$UID); // set the "uid" parameter to user's ID
		$request->setParam("data",$userArray );
    if(!$append)
    {
  		$request->setParam("updateBehavior",'arraySet');
    }
    else
    {
  		$request->setParam("updateBehavior",'arrayPush');
    }
		$response = $request->send();


		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return false;
		}

		return true;
	}


	//--------------------------------------------------------------------------
	// userGCSLogin
	// 	The method searches the user in GCS, if the email+password combination is found the method returns the user ID,
	// 	else the method returns -1.
	//--------------------------------------------------------------------------
	public function userGCSLogin($uEmail, $uPass){

		$sPassword = $this->generateHash($uPass);
		$searchQuery = 'select _id from _profile where email_s="' . $uEmail . '" and password_s="' . $sPassword . '"';
		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.search");
		$request->setParam("query",$searchQuery);
		$response = $request->send();


		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);  // write the Gigya's response log into the log file
			return -1;
		}

		if ($response->getInt("objectsCount", 0) == 0)
			return -1;

		$data = $response->getArray("data",null);
		$firstObj= $data->getObject(0);
		$UID = $firstObj->getString("_id",null);
		return $UID;
	}



  /**
   * updateProfile
   *
   */
  public function updateProfile($UID, $arrayData)
  {

    $user =  $this->getUserProfileData($UID);

    $pwd =  $this->generateHash($arrayData['password_s']);
    $arrayData['password_s'] = $pwd;
    $arrayData['email_s'] = $user['_email'];
    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.setUserData");
    $request->setParam("UID",$UID);
    $request->setParam("data",json_encode($arrayData));
    $response = $request->send();

    if($response->getErrorCode()!=0){
      // Error!
      $this->writeLog($response);
      return false;
    }

    return true;


  }


	//--------------------------------------------------------------------------
	// registerNewUser
	//   This method adds a new user record to the GCS.
	//--------------------------------------------------------------------------
	public function registerNewUser($UID, $email="", $password){

		$secure_pass = $this->generateHash($password);
		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.setUserData");
		$request->setParam("UID",$UID);
		$request->setParam("data", json_encode(array(
		  'password_s'  => $secure_pass,
			'email_s'     => $email,
			'status_i'    => 99,
      'confirmed'   => false,
      'confirmedCode' => $this->createRandomPassword()
    )));
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return false;
		}

		return true;
	}


	//--------------------------------------------------------------------------
	// isEmailUnique
	//   Check if the input email already exists in the GCS
	//--------------------------------------------------------------------------
	public function isEmailUnique($email){

		$searchQuery = 'select email_s from _profile where email_s="' . $email . '"';
		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.search");
		$request->setParam("query",$searchQuery);
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return false;
		}

		// if the response includes 0 objects => email is unique => return true
		return ($response->getInt("objectsCount", 0) == 0);
	}

	//--------------------------------------------------------------------------
	// retrieveUserByEmail
	//   retrieve the user's ID according to his email
	//--------------------------------------------------------------------------
	public function retrieveUserByEmail($email){

		$searchQuery = 'select _id from _profile where email_s="' . $email . '"';
		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gcs.search");
		$request->setParam("query",$searchQuery);
		$response = $request->send();
		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);  // write the Gigya's response log into the log file
			return $response->getErrorMessage(); // return the error message
		}

		if ($response->getInt("objectsCount", 0) == 0)
			return false;

		$userObject = $response->getString("data")->getObject(0);
		$UID = $userObject->getString("_id");
		return $UID;

	}

	//--------------------------------------------------------------------------
	// deleteUser
	//   This method deletes a User record from Gigya.
	//--------------------------------------------------------------------------
	public function deleteUser($UID){

		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.deleteAccount");
		$request->setParam("UID",$UID);
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return false;
		}

		return true;
	}

	//--------------------------------------------------------------------------
	// exprteUser
	//   This method export users record from Gigya.
	//--------------------------------------------------------------------------
	public function exportUsers(){

		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.exportUsers");
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return false;
		}

		return $response;
	}


  //--------------------------------------------------------------------------
  // getTopUsers
  // This method export Top Users of Gamification
  //
  // @params $which Wich challenge
  // @params $UID
  // @params $period (parameter type all|7days)
  // @params $howMany ;)
  //--------------------------------------------------------------------------
  public function getTopUsers( $which = "_default", $UID = null, $period = 'all', $howMany = 10){

    $aPeriod = (in_array($period, Array('all','7days'))) ? $period : 'all';

    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gm.getTopUsers");
    $request->setParam("period", $period);
    $request->setParam("challenge", $which);
    $request->setParam("totalCount", $howMany);
    $request->setParam("includeSelf", false);

    /** Deprecated 
    if(!is_null($UID))
    {
      $request->setParam("includeSelf", true);
      $request->setParam("UID", $UID);
    }
    else
    {
      $request->setParam("includeSelf", false);
    }
    Deprecated **/

    $response = $request->send();

    if($response->getErrorCode()!=0){
      // Error!
      $this->writeLog($response);
      return Array();
    }


    return json_decode($response->getArray('users')->toJsonString(), true);
  }


   //--------------------------------------------------------------------------
  // getActionsPoint
  // This method retrieves the configuration of points set by Gigya Dashboard
  // 
  // @params $which Which action
  //--------------------------------------------------------------------------
  public function getActionsPoint($which = "*") {

    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gm.getChallengeConfig");
    $request->setParam("IncludeChallenges", "*");
    $response = $request->send();

    if($response->getErrorCode()!=0){
      // Error!
      $this->writeLog($response);
      return Array();
    }

    $points    = json_decode($response->getArray('actions')->toJsonString(), true);

    $point = array();
    foreach($points as $aPoint)
    {
      $point[$aPoint['actionID']] = $aPoint;
    }

    if($which == '*') return $point;
    if(isset($point[$which])) return $point[$which];
    return $point;
  }

  //--------------------------------------------------------------------------
  // getChallenge
  // This method retrieves the configuration of specified challenges. The method returns the list of challenges, their levels and the associated action descriptions. Note: only active challenges and actions are returned.
  // 
  // @params $which Which challenge
  //--------------------------------------------------------------------------
  public function getChallenge($which = "*") {

    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gm.getChallengeConfig");
    $request->setParam("IncludeChallenges", $which);
    $response = $request->send();

    if($response->getErrorCode()!=0){
      // Error!
      $this->writeLog($response);
      return Array();
    }

    $challenges = json_decode($response->getArray('challenges')->toJsonString(), true);

    // Use this for set _default to the top of the list

    foreach($challenges as $challenge)
    {
      $_aChls[$challenge['challengeID']] = $challenge;
    }

    $aChls = array();
    if(isset($_aChls["_default"])) $aChls[0] = $_aChls["_default"];
    foreach($_aChls as $k => $_aChl)
    {
      if($k != '_default') array_push($aChls, $_aChl);
    }

    if(count($aChls) == 1) return current($aChls);
    return $aChls;
  }

  public static function objectToArray($d) {
    if (is_object($d)) {
      // Gets the properties of the given object
      // with get_object_vars function
      return get_object_vars($d);
    }
    else {
      // Return array
      return $d;
    }
  }


  //--------------------------------------------------------------------------
  // getUserAchievement
  // 
  // @params $user user array structure get From gm.getTopUser
  // @params $which Which challenge
  //--------------------------------------------------------------------------
  public static function getUserAchievement($_user, $which = "*") {
    $user = self::objectToArray($_user);
    if(!is_array($user)) return Array();
    if( $which == '*' ) return $user['achievements'];

    if( $which != '*')
    {
      foreach($user['achievements'] as $_achievement)
      {
        $achievement = self::objectToArray($_achievement);

        if($which == $achievement['challengeID']) return $achievement;
      }
    }

    return Array();

  }
  
   //--------------------------------------------------------------------------
  // getUserLevel
  // 
  // @params $user array structure get From gm.getTopUser
  // @params $challenge array structure get From gm.getChallengeConfig
  // @params $which Which Level
  //--------------------------------------------------------------------------
  public static function getUserLevel($user, $challenge,  $which = "*") {

    if(!is_array($challenge)) return Array();

    if(!is_array($user)) return Array();

    if( $which == '*') return $challenge['levels'];

    if( $which != '*' && ((count($challenge['levels'])-1) > $which ))
    {
      foreach($challenge['levels'] as $level)
      {
        if($which == $level['level']) return $level;
      }
    }
    else
    {
      return $challenge['levels'][count($challenge['levels'])-1];
    }

    return Array();

  }


	//------------------------------------------------------------------------------------------------------
	// Helper Methods
	//------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------

	//-----------------------------------------------------------------------------------------------------------
	// getSocialUserInfo
	//	Parameter:
	//		UID - the user's GCS ID
	//		extraUserData - you may request to retrieve extra fields, such as: likes, education, favorites, etc.
	//-----------------------------------------------------------------------------------------------------------
	public function getSocialUserInfo($UID, $extraUserData = "likes"){

		$request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.getUserInfo");
		$request->setParam("uid",$UID);  // set the "uid" parameter to user's ID
		$request->setParam("includeAllIdentities",true);
		$request->setParam("extraFields",$extraUserData);
		$response = $request->send();

		if($response->getErrorCode()!=0){
			// Error!
			$this->writeLog($response);
			return null;
		}

		$userObject=json_decode($response->getData());
		return $userObject;
	}
        
  //-----------------------------------------------------------------------------------------------------------
	// getSocialFriends
	//	Parameter:
	//		UID - the user's GCS ID
	//-----------------------------------------------------------------------------------------------------------
  public function getSocialFriends($UID){

      $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.getFriendsInfo");
      $request->setParam("uid",$UID);  // set the "uid" parameter to user's ID
      //$request->setParam("includeAllIdentities",true);
      //$request->setParam("detailLevel", "basic");
      //$request->setParam("extraFields",$extraUserData);
      $response = $request->send();

      if($response->getErrorCode()!=0){
              // Error!
              $this->writeLog($response);
              return null;
      }

      $userObject=json_decode($response->getData());

      return $userObject;
  }

  // Arguments for params: link, message, picture, name, caption, description
  public function inviteFriends($UID, $profileId, $params) {    
    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.sendNotification");
    
    $request->setParam("uid", $UID);
    $request->setParam("recipients", implode(',', $profileId));
    $request->setParam("body", $params['body']);
    $request->setParam("subject", $params['subject']);

    $response = $request->send();

    if($response->getErrorCode()!=0){
              $this->writeLog($response);
              return null;
      }

      $userObject=json_decode($response->getData());

      return $userObject;
  }

	//--------------------------------------------------------------------------
	// validateSignature
	//--------------------------------------------------------------------------
	public function validateSignature($UID, $signatureTimestamp,$UIDSignature){

		if(!SigUtils::validateUserSignature($UID,$signatureTimestamp,$this->gigyaSecret,$UIDSignature))
			return false;
		else
			return true;
	}


  //--------------------------------------------------------------------------
  // getUserAvailableProviders
  // The method retrieves user profile data from GCS.
  //--------------------------------------------------------------------------
  public function getUserAvailableProviders(){

    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"socialize.getAvailableProviders");
    $response = $request->send();
    if($response->getErrorCode()==0){
      $userObject = json_decode($response->getString('availableProviders'),true);
      return $userObject;
    }
    else { // Error!
      $this->writeLog($response);
      return null;
    }
  }

//--------------------------------------------------------------------------
  // getUserGamification
  // The method retrieves user gamification data from GameMechanism
  //--------------------------------------------------------------------------
  public function getUserGamification($guid)
  {
    if(empty($guid)) return null;
    $request = new GSRequest($this->gigyaApiKey,$this->gigyaSecret,"gm.getChallengeStatus");
    $request->setParam("UID",$guid);
    $request->setParam("details",'full');
    $response = $request->send();
    if($response->getErrorCode()==0){
      $userObject = json_decode($response->getResponseText());
      return $userObject;
    }
    else { // Error!
      $this->writeLog($response);
      return Array();
    }

    return Array();
  }

  /**
   * Method: notifyAction
   */
  public function notifyAction($uid, $actionID)
  {

   $gsRequest = new GSRequest($this->gigyaApiKey,$this->gigyaSecret, 'gm.notifyAction');
   $gsRequest->setParam("uid", $uid);
   $gsRequest->setParam("action", $actionID);
   $gsResponse = $gsRequest->send();

   if($gsResponse->getErrorCode()==0){
     $userObject = json_decode($gsResponse->getResponseText());
     return $userObject;
   }
   else { // Error!
    $err = new stdClass;
    $err->statusCode = false;
    $err->actionCalled = $actionID;
    if(empty($uid))
    {
      $err->statusReason = "User not found";
    }
    else
    {
      $err->statusReason = "Action not found";
    }
    $err->callId = '';
    
    return $err;
    return $gsResponse->getLog(); 
   }
  }



  //--------------------------------------------------------------------------
	// generateHash
	// 	The method generates a Hash for a given string.
	//--------------------------------------------------------------------------
	public function generateHash($plainText){
		$salt = substr(md5($plainText), 0, 32);
		return $salt;
	}

	public function createRandomPassword() {
	    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
	    srand((double)microtime()*1000000);
	    $i = 0;
	    $pass = '' ;
	    while ($i <= 7) {
	        $num = rand() % 33;
	        $tmp = substr($chars, $num, 1);
	        $pass = $pass . $tmp;
	        $i++;
	    }
	    return $pass;
	}



}

?>
