<?php

class PluginSfGigyaUserAcceptRulesForm extends baseForm
{

  public static $rules     = Array('2'=>'Non accetto','1'=>'Accetto');

  public function configure()
  {
    $this->setWidgets(array(
      'rules'       => new sfWidgetFormChoice(array(
        'choices' => self::$rules,
        'expanded' => true,
        'renderer_options' => array('formatter' => array('PluginSfGigyaUserAcceptRulesForm', 'MyFormatter'))
      )),
    ));

    $this->setValidators(array(
      'rules'        => new sfValidatorChoice(array('required'=>true, 'choices'=>array_keys(self::$rules))),
    ));

    $this->widgetSchema->setNameFormat('sfGigyaUserAcceptRules[%s]');
  }

  public static function MyFormatter($widget, $inputs) {
    asort($inputs);
    $k = 0;
    foreach ($inputs as $input) {
      $pos = ($k==0) ? 'left' : 'right';
      $label = ($k==0) ? 'Accetto il regolamento' : 'Non accetto il regolamento';
      $result .= '<div class="radio ' . $pos .'">' . $input['input'] . '&nbsp;' . $label . '</div>';
      ++$k;
    }
    return $result;
  }

}
