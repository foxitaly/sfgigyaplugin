
<?php

/**
 * PluginsfGigyaUserChangePasswordForm
 * @package    symfony
 * @subpackage form
 */
class PluginsfGigyaUserChangePasswordForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'password_s'         => new sfWidgetFormInputPassword(),
      'repassword_s'       => new sfWidgetFormInputPassword(),
    ));

    $this->setValidators(array(
      'password_s'         => new sfValidatorString(array('min_length' => 8), array('min_length' => 'Password is too short (%min_length% characters min).', 'required' => 'Your password is required.')),
      'repassword_s'       => new sfValidatorString(array(), array('required' => 'Your password confirmation is required.')),
    ));

    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorSchemaCompare('password_s', sfValidatorSchemaCompare::EQUAL, 'repassword_s', array(), array('invalid' => 'The two passwords do not match')),
    )));

    $this->widgetSchema->setNameFormat('sfGigyaUserChangePassword[%s]');
  }
}
