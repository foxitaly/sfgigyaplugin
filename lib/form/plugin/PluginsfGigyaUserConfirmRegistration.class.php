<?php

class PluginSfGigyaUserConfirmRegistrationForm extends baseForm
{

  public function configure()
  {

    $this->setWidgets(array(
      'code'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'code'     => new sfValidatorString(array('required'=>true,'trim' => true)),
    ));

    $this->widgetSchema->setNameFormat('sfGigyaUserConfirmRegistration[%s]');
  }

}
