
<?php

class PluginSfGigyaUserLinkForm extends baseForm
{
  public function configure()
  {
     $this->disableLocalCSRFProtection();

      $this->setWidgets(array(
      'email'            => new sfWidgetFormInputText(),
      'password_s'       => new sfWidgetFormInputPassword(),
      'gcsLogin'         => new sfWidgetFormInputHidden()
    ));

    $this->widgetSchema->setNameFormat('sfGigyaUserLink[%s]');

    $this->widgetSchema['password_s']->setLabel('password');

    $this->setValidators(array(
      'email'            => new sfValidatorAnd (Array(
        new sfValidatorEmail(array('trim' => true), array('required' => 'Your e-mail address is required.', 'invalid' => 'The email address is invalid.'))
      )),
      'password_s'       => new sfValidatorString(array('min_length' => 8, 'required'=>false), array('min_length' => 'Password is too short (%min_length% characters min).', 'required' => 'Your password is required.')),
      'gcsLogin'         => new sfValidatorString(array('required' => 'gcs error.')),
      
    ));


  }

}
