
<?php

class PluginSfGigyaUserLinkNoMailForm extends baseForm
{
  public function configure()
  {
    $this->disableLocalCSRFProtection();

    $this->setWidgets(array(
      'email'            => new sfWidgetFormInputText(),
      'gcsLogin'         => new sfWidgetFormInputHidden()

    ));

    $this->widgetSchema->setNameFormat('sfGigyaUserLinkNoMail[%s]');

    $this->setValidators(array(
      'email'            => new sfValidatorAnd (Array(
        new sfValidatorEmail(array('trim' => true), array('required' => 'Your e-mail address is required.', 'invalid' => 'The email address is invalid.'))
      )),
      'gcsLogin'         => new sfValidatorString(array('required' => 'gcs error.')),
    ));


  }

}
