<?php

/**
 * PluginsfGigyaUserLoginForm
 * @package    symfony
 * @subpackage form
 */
class PluginsfGigyaUserLoginForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'email'            => new sfWidgetFormInputText(),
      'password'         => new sfWidgetFormInputPassword(),
      'ref'               => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'email'            => new sfValidatorAnd (Array(
        new sfValidatorEmail(array('trim' => true), array('required' => 'Your e-mail address is required.', 'invalid' => 'The email address is invalid.')),
      )),
      'password'         => new sfValidatorString(array('min_length' => 8), array('min_length' => 'Password is too short (%min_length% characters min).', 'required' => 'Your password is required.')),
      'ref'             => new sfValidatorPass(Array('required'=>false))
    ));

    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorGigyaUserExists('email', 'password', array(), array('invalid' => 'Invalid credentials')),
    )));
 

    $ref = ($this->getOption('ref',"/") == '') ? "/" : $this->getOption('ref',"/");
    $this->widgetSchema['ref']->setDefault($ref);

    $this->widgetSchema->setNameFormat('sfGigyaUserLogin[%s]');
  }
}
