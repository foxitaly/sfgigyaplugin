
<?php

/**
 * PluginsfGigyaUserLostPasswordForm
 * @package    symfony
 * @subpackage form
 */
class PluginsfGigyaUserLostPasswordForm extends BaseForm
{
  public function configure()
  {
    $this->widgetSchema['email_s']    = new sfWidgetFormInput();

    $this->setValidators(array(
      'email_s'            => new sfValidatorAnd (Array(
        new sfValidatorEmail(array('trim' => true), array('required' => 'Your e-mail address is required.', 'invalid' => 'The email address is invalid.')),
        new sfValidatorGigyaUserEmailExist()
      ))
    ));

    $this->widgetSchema->setNameFormat('sfGigyaUserLostPassword[%s]');
  }
}
