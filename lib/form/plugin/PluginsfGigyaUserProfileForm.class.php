<?php

class PluginSfGigyaUserProfileForm extends baseForm
{

  public static $gender     = Array('m'=>'Maschio','f'=>'Femmina');
  public static $nlChnls    = Array('fox'=>'Foxtv','foxlife'=>'Foxlife','Foxcrime'=>'Foxcrime','foxretro'=>'Foxretro','natgeo'=>'NatgeoTv','history'=>'Historychannel');

  static protected $province = Array ('' => 'Scegli','AG' => 'Agrigento', 'AL' => 'Alessandria','AN' => 'Ancona','AO' => 'Aosta','AR' => 'Arezzo','AP' => 'Ascoli Piceno','AT' => 'Asti', 'AV' => 'Avellino', 'BA' => 'Bari', 'BT' => 'Barletta-Andria-Trani', 'BL' => 'Belluno', 'BN' => 'Benevento', 'BG' => 'Bergamo', 'BI' => 'Biella', 'BO' => 'Bologna', 'BZ' => 'Bolzano', 'BS' => 'Brescia', 'BR' => 'Brindisi', 'CA' => 'Cagliari', 'CL' => 'Caltanissetta', 'CB' => 'Campobasso', 'CI' => 'Carbonia-Iglesias', 'CE' => 'Caserta', 'CT' => 'Catania', 'CZ' => 'Catanzaro', 'CH' => 'Chieti', 'CO' => 'Como', 'CS' => 'Cosenza', 'CR' => 'Cremona', 'KR' => 'Crotone', 'CN' => 'Cuneo', 'EN' => 'Enna', 'FM' => 'Fermo', 'FE' => 'Ferrara', 'FI' => 'Firenze', 'FG' => 'Foggia', 'FC' => 'Forlì-Cesena', 'FR' => 'Frosinone', 'GE' => 'Genova', 'GO' => 'Gorizia', 'GR' => 'Grosseto', 'IM' => 'Imperia', 'IS' => 'Isernia', 'SP' => 'La Spezia', 'AQ' => 'L\'Aquila', 'LT' => 'Latina', 'LE' => 'Lecce', 'LC' => 'Lecco', 'LI' => 'Livorno', 'LO' => 'Lodi', 'LU' => 'Lucca', 'MC' => 'Macerata', 'MN' => 'Mantova', 'MS' => 'Massa-Carrara', 'MT' => 'Matera', 'ME' => 'Messina', 'MI' => 'Milano',  'MO' => 'Modena',  'MB' => 'Monza e della Brianza', 'NA' => 'Napoli', 'NO' => 'Novara', 'NU' => 'Nuoro', 'OT' => 'Olbia-Tempio', 'OR' => 'Oristano', 'PD' => 'Padova','PA' => 'Palermo', 'PR' => 'Parma', 'PV' => 'Pavia', 'PG' => 'Perugia', 'PU' => 'Pesaro e Urbino', 'PE' => 'Pescara', 'PC' => 'Piacenza', 'PI' => 'Pisa', 'PT' => 'Pistoia', 'PN' => 'Pordenone', 'PZ' => 'Potenza', 'PO' => 'Prato', 'RG' => 'Ragusa', 'RA' => 'Ravenna', 'RC' => 'Reggio', 'RE' => 'Reggio Emilia', 'RI' => 'Rieti', 'RN' => 'Rimini', 'RM' => 'Roma', 'RO' => 'Rovigo', 'SA' => 'Salerno', 'VS' => 'Medio Campidano', 'SS' => 'Sassari', 'SV' => 'Savona', 'SI' => 'Siena', 'SR' => 'Siracusa', 'SO' => 'Sondrio', 'TA' => 'Taranto', 'TE' => 'Teramo', 'TR' => 'Terni',  'TO' => 'Torino', 'OG' => 'Ogliastra', 'TP' => 'Trapani', 'TN' => 'Trento','TV' => 'Treviso', 'TS' => 'Trieste', 'UD' => 'Udine', 'VA' => 'Varese', 'VE' => 'Venezia', 'VB' => 'Verbano-Cusio-Ossola',  'VC' => 'Vercelli',  'VR' => 'Verona',  'VV' => 'Vibo Valentia',  'VI' => 'Vicenza', 'VT' => 'Viterbo');
  static protected $sky =  Array ('0' => 'No', '1'=>'Si');


  public function configure()
  {

    $years = range('1940',date('Y')); //Creates array of years between 1920-2000 
    $years_list = array_combine($years, $years); //Creates new array where key and value are both values from $years list

    $defChannel = strtolower(ProjectConfiguration::getThisChannel());

    $this->setWidgets(array(
      'firstName'     => new sfWidgetFormInputText(),
      'lastName'      => new sfWidgetFormInputText(),
      'mobileNum_s'   => new sfWidgetFormInputText(),
      'city'          => new sfWidgetFormInputText(),
      'email_s'       => new sfWidgetFormInputHidden(),
      'gender'       => new sfWidgetFormChoice(array(
        'choices' => self::$gender
      )),
      'sky' => new sfWidgetFormChoice(array(
        'choices'   => self::$sky,
        'expanded'  => true
      )),

      'newsletter_channels' => new sfWidgetFormChoice(array(
        'choices'   => self::$nlChnls,
        'expanded'  => true,
        'multiple'  => true
      )),
      'birthday'            => new sfWidgetFormDate(Array(
        'format' => "%day%/%month%/%year%",
        'years'   => $years_list
      )),
      'channel'             => new sfWidgetFormInputHidden(),
      'privacy'             => new sfWidgetFormInputCheckbox(),
      'address'             => new sfWidgetFormInputText(),  
      'province'            => new sfWidgetFormChoice(array(
        'choices' => self::$province
      )),


    ));

    $this->setValidators(array(
      'email_s'       => new sfValidatorEmail(array('required'=>false,'trim' => true)),
      'lastName'      => new sfValidatorString(array('required'=>true,'trim' => true)),
      'firstName'     => new sfValidatorString(array('required'=>true,'trim' => true)),
      'mobileNum_s'   => new sfValidatorString(array('required'=>false,'trim' => false)),
      'city'          => new sfValidatorString(array('required'=>false,'trim' => false)),
      'gender'        => new sfValidatorChoice(array('required'=>false, 'choices'=>array_keys(self::$gender))),
      'sky'           => new sfValidatorChoice(array('required'=>true, 'choices'=>array_keys(self::$sky))),
      'newsletter_channels' => new sfValidatorChoice(array('required'=>false, 'multiple' => true, 'choices'=>array_keys(self::$nlChnls))),
      'channel'             => new sfValidatorString(array('required'=>false)),
      'birthday'            => new sfValidatorDate(array('required'=>false)),
      'privacy'             => new sfValidatorBoolean(array('required'=>true)),
      'channel'             => new sfValidatorString(array('required'=>false)),
      'province'            => new sfValidatorChoice(array('required'=>false, 'choices'=>array_keys(self::$province))),
      'address'             => new sfValidatorString(array('required'=>false))
    ));

    $sfUser = $this->getOption('sfUser');

    if($sfUser->hasGigyaSecurityUser())
    {
      $this->setDefault('newsletter_channels',Array($defChannel));
      $this->setDefault('firstName',$sfUser->getFirstName());
      $this->setDefault('lastName',$sfUser->getLastName());
      $this->setDefault('mobileNum_s',$sfUser->getMobile());
      $this->setDefault('gender',$sfUser->getGender());
      $this->setDefault('city',$sfUser->getCity());
      $this->setDefault('email_s',$sfUser->getEmail_s());
      $this->setDefault('newsletter_s',$sfUser->getNewsletter_s());
      $this->setDefault('sky',$sfUser->getSky());

      if($sfUser->getAttribute('userMustBeCompleteData'))
      {
        $defNL = ($sfUser->getNewsletterChannels()) ? $sfUser->getNewsletterChannels() : Array($defChannel);
      }
      else
      {
        $defNL = ($sfUser->getNewsletterChannels()) ? $sfUser->getNewsletterChannels() : Array();
      }
      $this->setDefault('newsletter_channels',$defNL);
      $this->setDefault('birthday',$sfUser->getBirthday());
      $this->setDefault('channel',  $sfUser->getChannel());
      $this->setDefault('province',  $sfUser->getProvince());
      $this->setDefault('address',  $sfUser->getAddress());
    }
    else
    {
      $this->setDefault('channel',  $defChannel);
      $this->setDefault('newsletter_channels',Array($defChannel));
    }
 
    if(!($sfUser->getAttribute('fromSignup',false) || $sfUser->getAttribute('userMustBeCompleteData',false)))
    {
      unset($this['privacy']);
    }

 

    $this->widgetSchema->setNameFormat('sfGigyaUserProfile[%s]');
  }

}
