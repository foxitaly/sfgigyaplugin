<?php

/**
 * PluginsfGigyaUserSignupForm
 * @package    symfony
 * @subpackage form
 */
class PluginsfGigyaUserSignupForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'email'            => new sfWidgetFormInputText(),
      'password'         => new sfWidgetFormInputPassword(),
      'repassword'       => new sfWidgetFormInputPassword(),
    ));

    $this->setValidators(array(
      'email'            => new sfValidatorAnd (Array(
        new sfValidatorEmail(array('trim' => true), array('required' => 'Your e-mail address is required.', 'invalid' => 'The email address is invalid.')),
        new sfValidatorGigyaEmail()
      )),
      'password'         => new sfValidatorString(array('min_length' => 8), array('min_length' => 'Password is too short (%min_length% characters min).', 'required' => 'Your password is required.')),
      'repassword'       => new sfValidatorString(array(), array('required' => 'Your password confirmation is required.')),
    ));

    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'repassword', array(), array('invalid' => 'The two passwords do not match')),
    )));

    $this->widgetSchema->setNameFormat('sfGigyaUserSignup[%s]');
  }
}
