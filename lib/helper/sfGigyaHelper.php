<?php

/**
 * sf_gigya_url_login
 *
 * @return the Login Url
 */
function sf_gigya_url_login()
{
  return url_for('@sfGigyaLogin');
}

/**
 * sf_gigya_url_logout
 *
 * @return the Logout Url
 */
function sf_gigya_url_logout()
{
  return url_for('@sfGigyaLogout');
}

/**
 * sf_gigya_url_landing
 *
 * @return the Landing URL
 */
function sf_gigya_url_landing()
{
  return url_for('@sfGigyaLandingGamification');
}


/**
 * sf_gigya_url_user_signup
 *
 * @return the Signup Url
 */
function sf_gigya_url_user_signup()
{
  return url_for('@sfGigyaSignupUser');
}

/**
 * sf_gigya_url_user_register
 *
 * @return the Register Url
 */
function sf_gigya_url_user_register()
{
  return url_for('@sfGigyaRegisterUser'); 
}


/**
 * sf_gigya_url_user_profile
 *
 * @return the Profile Url
 */
function sf_gigya_url_user_profile()
{
  return url_for('@sfGigyaProfileUser');
}

/**
 * sf_gigya_url_user_change_password
 *
 * @return the Profile Url
 */
function sf_gigya_url_user_change_password()
{
  return url_for('@sfGigyaChangePasswordUser');
}

/**
 * sf_gigya_url_user_lost_password
 *
 * @return the Lost password Url
 */
function sf_gigya_url_user_lost_password()
{
  return url_for('@sfGigyaForgotPasswordUser');
}

/**
 * sf_gigya_url_accept_rules
 *
 * @return the accept rules Url
 */
function sf_gigya_url_accept_rules()
{
  return url_for('@sfGigyaAcceptRulesUser');
}
/**
 * errorClass
 *
 * @params boolean
 *
 */
function errorClass($flag)
{
  if($flag) return 'sfGigyaErrorForm';
  return '';
}
