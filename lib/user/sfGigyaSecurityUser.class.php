<?php

/**
 *
 * @package    sfGigyaPlugin
 * @subpackage plugin
 * @author     Mauro D'Alatri
 */
class sfGigyaSecurityUser extends sfBasicSecurityUser
{
  protected $user             = null;
  protected $userProfileData  = null;
  protected $UID;
  protected $gigyaData        = "languages, address, phones, education, honors, publications, patents, certifications, professionalHeadline, bio, industry, specialties, work, skills, religion, politicalView, interestedIn, relationshipStatus, hometown, favorites, username, locale, verified";


  public function initialize(sfEventDispatcher $dispatcher, sfStorage $storage, $options = array())
  {
    parent::initialize($dispatcher, $storage, $options);

    $this->gigyaWrap = new GigyaWrap();
  }


  /**
   * isMyGUID
   *
   * @parameters string $guid
   */
  public function isMyGUID($guid)
  {
    return ($this->getUID() == $guid);
  }
   
  /**
   * isFirstRequest
   *
   * @params boolean $boolean
   *
   */

  public function isFirstRequest($boolean = null)
  {
    if (is_null($boolean))
    {
      return $this->getAttribute('first_request', true);
    }
    else
    {
      $this->setAttribute('first_request', $boolean);
    }
  }


  public function comeFromSignupOrMustBeCompleteData()
  {
    return ($this->getAttribute('fromSignup') || $this->getAttribute('userMustBeCompleteData'));
  }


  public function getSocialUserInfo($UID)
  {
    if($UID == '') return false;
    return $this->gigyaWrap->getSocialUserInfo($UID, $this->gigyaData);
  }

  // Retrieve the social friends of the user
  // This was added for the Contest 125 plugin
  public function getFriends($UID)
  {
      if($UID == '') return false;
      return $this->gigyaWrap->getSocialFriends($UID);
  }

  // Call the standard Gigya invitation function
  // This was added for the Contest 125 plugin
  public function inviteFriends($UID, $profileId, $params)
  {
      if($UID == '') return false;
      return $this->gigyaWrap->inviteFriends($UID, $profileId, $params);
  }


  public function validateSignature($UID)
  {
    $sfGigyaUser = $this->getSocialUserInfo($UID);
    if($sfGigyaUser)
    {
      $valid = $this->gigyaWrap->validateSignature($sfGigyaUser->UID, $sfGigyaUser->signatureTimestamp, $sfGigyaUser->UIDSignature);

      if ($valid)
      {
        return $sfGigyaUser;
      }
    }
    return false;

  }

  public function isEmailUnique($email)
  {
    return $this->gigyaWrap->isEmailUnique($email);
  }


  public function verifySSO($UID)
  {

    if($sfGigyaUser = $this->validateSignature($UID))
    {

      if ($sfGigyaUser->isLoggedIn)
      {
        $this->signin($sfGigyaUser);
        return true;
      }
      else
      {
        $this->setAuthenticated(false);
        return false;
      }
    }
    else
    {
      $this->setAuthenticated(false);
      return false;
    }
  }

  public function signin($sfGigyaUser)
  {

    $this->setGigyaSecurityUser($sfGigyaUser);
    $aUID = (isset($sfGigyaUser->UID))  ? $sfGigyaUser->UID : $this->getAttribute('gcsLogin');
    $this->setGigyaUserProfileData($this->gigyaWrap->getUserProfileData($aUID));

    //$this->setGigyaUserProfileData($this->gigyaWrap->getUserProfileData($this->getAttribute('gcsLogin')));
    $this->setAuthenticated(true);
  }

  public function signout()
  {

    $aGcsLogin  = $this->getAttribute('gcsLogin',false);    
    $aReferer   = $this->getAttribute('gigyaRedirectUrl',false);

    $this->gigyaWrap->notifySiteLogout(sfConfig::get('app_gigya_apikey'),sfConfig::get('app_gigya_secretkey'), $this->getUID());
    $this->getAttributeHolder()->clear();
    $this->setAuthenticated(false);

    if($aGcsLogin)  $this->setAttribute('gcsLogin',$aGcsLogin);
    if($aReferer)   $this->setAttribute('gigyaRedirectUrl',$aReferer);
    

    return true;
  }

  public function setGigyaSecurityUser($gigyaUser)
  {
    $this->setAttribute('sfGigyaSecurityUser',$gigyaUser);
  }

  public function setGigyaUserProfileData($gigyaUserProfileData)
  {
    $this->setAttribute('sfGigyaUserProfileData',$gigyaUserProfileData);
  }


  public function getGigyaSecurityUser()
  {
    $this->user = $this->getAttribute('sfGigyaSecurityUser', false);

    if(!$this->user)
    {
      return $this->getSocialUserInfo($this->getUID());
    }

    return $this->user;
  }

  public function getGigyaUserProfileData($key = '')
  {
    if(!$this->getUID()) return null;
  

    if (!$this->hasAttribute('sfGigyaUserProfileData'))
    {
      $this->setAttribute('sfGigyaUserProfileData', $this->gigyaWrap->getUserProfileData($this->getUID()));
    }
    $this->userProfileData = $this->getAttribute('sfGigyaUserProfileData');

    return ($key != '' && isset($this->userProfileData[$key])) ? $this->userProfileData[$key] : null;
  }



  public function hasGigyaSecurityUser()
  {
    if($this->hasAttribute('sfGigyaSecurityUser') && $this->getAttribute('sfGigyaSecurityUser', '') != '') return true;
    return false;
  }

  /**
   * Returns true if user is authenticated.
   *
   * @return boolean
   */
  public function isAuthenticated()
  {

    /* RALLENTA TUTTO IL SITO DA VERE COME GESTIRE AL MEGLIO
    if($UID = $this->getUID())
    {
      return $this->verifySSO($UID);
    }
    */
    return $this->authenticated;
  }


  /**
   * Persist Gigya UID On GigyaUser table
   *
   */
  public function persistMyGigyaUID()
  {

      $persist = GigyaUserQuery::create()->filterByUid($this->getUID())->findOneOrCreate();
    
      if($persist->getLastLogin('Y-m-d') < date('Y-m-d'))
      {
        $persist->setLastLogin(date('Y-m-d H:i:s'));
      }

      $persist->save();

      return $persist;
  }

  /**
   * getMyGamification
   *
   */
  public function getMyGamification()
  {
    $data = $this->gigyaWrap->getUserGamification($this->getUID());
    return $data;
  }

  /**
   * getMyRank()
   *
   */
  public function getMyChallengeData($challengeID = "_default")
  {
    $dataGamification   = array();
    $aUserGamification  = $this->getMyGamification(); 
    $dataGamification   = gigyaWrap::getUserAchievement($aUserGamification, $challengeID);
    return $dataGamification;
  }


  /**
   * getMyTopRanking
   *
   */
  public function getMyTopRanking($gigyaChallengeID = "_default")
  {

    $ranking            = sfConfig::get('app_gigya_defaultRanking',10);
    $myChallengeData    = $this->getMyChallengeData($gigyaChallengeID);
    $myRank             = $myChallengeData['rank'];

    $configRank = sfConfig::get('app_gigya_topRank',array());
    if(isset($configRank[$gigyaChallengeID]))
    {
      $ranking = $configRank[$gigyaChallengeID];
    }

    $ret = array('topRanker' => ($myRank <= $ranking), 'myRank' => $myRank, 'ranking' => $ranking);

    return $ret;
  }

  // add some proxy method

  public function __toString()
  {
    return $this->user->getGigyaSecurityUser()->UID;
  }

  public function getThumbnailURL()
  {
    return (isset($this->getGigyaSecurityUser()->thumbnailURL) ) ? $this->getGigyaSecurityUser()->thumbnailURL : false ;
  }

  public function isSiteUID()
  {
    return $this->getGigyaSecurityUser()->isSiteUID;
  }

  public function getUID()
  {
    return ($this->hasGigyaSecurityUser())  ? $this->getGigyaSecurityUser()->UID : null;
  }

  public function getPhotoUrl()
  {
    return @$this->getGigyaSecurityUser()->identities[0]->photoURL;
  }

  public function getFirstName()
  {
    return ($this->getGigyaUserProfileData('firstName'))  ? $this->getGigyaUserProfileData('firstName') : @$this->getGigyaSecurityUser()->firstName;
  }

  public function getLastName()
  {
    return ($this->getGigyaUserProfileData('lastName'))  ? $this->getGigyaUserProfileData('lastName') :  @$this->getGigyaSecurityUser()->lastName;
  }

  public function getNickname()
  {
    return ($this->getGigyaUserProfileData('nickname'))  ?  $this->getGigyaUserProfileData('nickname') : @$this->getGigyaSecurityUser()->nickname;
  }

  public function getCountry()
  {
    return ($this->getGigyaUserProfileData('country'))  ?  $this->getGigyaUserProfileData('country') : @$this->getGigyaSecurityUser()->country;
  }

  public function getGender()
  {
    return ($this->getGigyaUserProfileData('gender'))  ?  $this->getGigyaUserProfileData('gender') : @$this->getGigyaSecurityUser()->gender;
  }

  public function getEmail()
  {
    return ($this->getGigyaUserProfileData('email'))  ?  $this->getGigyaUserProfileData('email') : @$this->getGigyaSecurityUser()->email;
  }

  public function getEmail_s()
  {
    return ($this->getGigyaUserProfileData('email_s'))  ?  $this->getGigyaUserProfileData('email_s') : @$this->getEmail();
  }

  public function getAddress()
  {
    return ($this->getGigyaUserProfileData('address'))  ?  $this->getGigyaUserProfileData('address') : @$this->getGigyaSecurityUser()->address;
  }

  public function getProvince()
  {
    return ($this->getGigyaUserProfileData('province'))  ?  $this->getGigyaUserProfileData('province') : @$this->getGigyaSecurityUser()->province;
  }

  public function getMobile()
  {
    return ($this->getGigyaUserProfileData('mobileNum_s'))  ?  $this->getGigyaUserProfileData('mobileNum_s') : @$this->getGigyaSecurityUser()->mobileNum_s;
  }

  public function getCity()
  {
    return ($this->getGigyaUserProfileData('city'))  ?  $this->getGigyaUserProfileData('city') : @$this->getGigyaSecurityUser()->city;
  }

  public function getNewsletter_s()
  {
    return ($this->getGigyaUserProfileData('newsletter_s'))  ?  $this->getGigyaUserProfileData('newsletter_s') : false;
  }

  public function getSky()
  {
    return ($this->getGigyaUserProfileData('sky'))  ?  $this->getGigyaUserProfileData('sky') : false;
  }

  public function getNewsletterChannels()
  {

    return ($this->getGigyaUserProfileData('newsletter_channels'))  ?  $this->getGigyaUserProfileData('newsletter_channels') : false;
  }

  public function getChannel()
  {
    return ($this->getGigyaUserProfileData('channel'))  ?  $this->getGigyaUserProfileData('channel') : false;
  }

  public function getBirthday()
  {
    return ($this->getGigyaUserProfileData('birthday'))  ?  $this->getGigyaUserProfileData('birthday') : false;
  }

  public function getPrivacy()
  {
    return ($this->getGigyaUserProfileData('privacy'))  ?  $this->getGigyaUserProfileData('privacy') : false;
  }

  public function isSiteProvider()
  {
    return ($this->getLoginProvider == 'site');
  }

  public function getLoginProvider()
  {
    return @$this->getGigyaSecurityUser()->loginProvider;
  }

  public function getAvailableProviders()
  {
   return $this->gigyaWrap->getUserAvailableProviders();
  }

  public function getPersistedGigyaUser()
  {
    return GigyaUserQuery::create()->findOneByUid($this->getUID());

  }

  public function isConfirmed($UID)
  {
    $gigyaUserProfileData = $this->gigyaWrap->getUserProfileData($UID);

    return  (isset($gigyaUserProfileData['confirmed'])) ? $gigyaUserProfileData['confirmed'] : false ;
  }

  public function hasAcceptedRules()
  {

    if(!$this->isAuthenticated()) return null;

    //$gigyaUser = GigyaUserQuery::create()->findOneByUid($this->getUID());
    $gigyaUser = $this->getPersistedGigyaUser();

    if($gigyaUser)
    {
      if ($gigyaUser->getRules() == 1) return true;
      if ($gigyaUser->getRules() == 2) return false;
      if ($gigyaUser->getRules() == 0) return null;
    } 
  
    return null; 
  }

  public function hasNotYetAcceptedRules()
  {
    return is_null($this->hasAcceptedRules());
  }

  public function isFirstTodayLogin()
  {

    $gigyaUser = $this->getPersistedGigyaUser();

    if($gigyaUser)
    {
      if($gigyaUser->getLastLogin('Ymd') == date('Ymd'))
      {
        if(date('YmdHis', strtotime($gigyaUser->getLastLogin('Y/m/d H:i:s') . " + 5 seconds")) > date('YmdHis')) return true;
        return false;
      }
      else
      {
        return true;
      }
    }
    return false;
  }

  public function getIdentities()
  {
    /* GO TO GIGYA AND VERIFY IF USER HAS NEW IDENTITIES LINKED (USED FOR ADDCONNECTION) */
    $gigyaSocialUserInfo = $this->getSocialUserInfo($this->getUID());
    $this->setGigyaSecurityUser($gigyaSocialUserInfo);

    $ids = @$this->getGigyaSecurityUser()->identities;
    if(is_array($ids)) return $ids;
    return Array();
  }

  public function hasIdentity($which = '')
  {
    if(empty($which)) return false;

    foreach($this->getIdentities() as $identity)
    {
      if (strtolower($identity->provider) == strtolower($which)) return true;
    }
    return false;
  }


}
