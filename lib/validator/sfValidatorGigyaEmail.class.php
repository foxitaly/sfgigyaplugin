<?php

class sfValidatorGigyaEmail  extends sfValidatorBase
{
  protected function configure($options = array(), $messages = array())
  {
  }

  protected function doClean($value)
  {
    $gWrap = new GigyaWrap();
    if(!$gWrap->isEmailUnique($value)) throw new sfValidatorError($this, 'exist', array('value' => $value));

    return $value;
  }

  public function isEmpty($value)
  {
    return false;
  }
}
