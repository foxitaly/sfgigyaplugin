<?php

class sfValidatorGigyaUserEmailExist  extends sfValidatorBase
{
  protected function configure($options = array(), $messages = array())
  {
  }

  protected function doClean($value)
  {
    $gWrap = new GigyaWrap();
    if(!$gWrap->retrieveUserByEmail($value)) throw new sfValidatorError($this, 'User not exist', array('value' => $value));

    return $value;
  }

  public function isEmpty($value)
  {
    return false;
  }
}
