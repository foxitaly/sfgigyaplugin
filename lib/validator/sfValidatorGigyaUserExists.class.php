<?php

class sfValidatorGigyaUserExists  extends sfValidatorSchema
{

  public function __construct($login, $password, $options = array(), $messages = array())
  {
    $this->addOption('login', $login);
    $this->addOption('password', $password);

    parent::__construct(null, $options, $messages);
  }


  protected function doClean($values)
  {

    if (null === $values)
    {
      $values = array();
    }

    if (!is_array($values))
    {
      throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
    }


    $aLogin = $values[$this->getOption('login')];
    $aPass  = $values[$this->getOption('password')];

    $gWrap = new GigyaWrap();

    $UID = $gWrap->userGCSLogin($aLogin, $aPass);
    if($UID == -1)
    {

      $error = new sfValidatorError($this, 'invalid', array(
        'login'  => $aLogin,
      ));
      throw new sfValidatorErrorSchema($this, array($this->getOption('login') => $error));

    }
  

    return $values;
  }
}
