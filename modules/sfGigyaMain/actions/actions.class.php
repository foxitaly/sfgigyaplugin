<?php

require_once dirname(__FILE__).'/../lib/BasesfGigyaMainActions.class.php';

/**
 * sfGigyaMain actions.
 * 
 * @package    sfGigyaPlugin
 * @subpackage sfGigyaMain
 * @author     Mauro DAlatri
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfGigyaMainActions extends BasesfGigyaMainActions
{
}
