<?php

/**
 * Base actions for the sfGigyaPlugin sfGigyaMain module.
 * 
 * @package     sfGigyaPlugin
 * @subpackage  sfGigyaMain
 * @author      Mauro DAlatri
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfGigyaMainActions extends sfActions
{

  /**
   * execute gamification
   *
   */
  public function executeGamification(sfWebRequest $request)
  {
    if(basename($request->getReferer()) == 'signup') $this->redirect('@homepage');
  }


  /**
   * execute rules
   *
   */
  public function executeRules(sfWebRequest $request)
  {
  }

  /**
   * execute awards
   *
   */
  public function executeAwards(sfWebRequest $request)
  {
  }

  /**
   * execute howto
   *
   */
  public function executeHowto(sfWebRequest $request)
  {
  }




}
