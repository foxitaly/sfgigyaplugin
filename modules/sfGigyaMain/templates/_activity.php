<script type="text/javascript">  

  // Publish an Activity Feed internally (only to site scope)  
  // This method is associated with the "btnPublishAction" click  
  function publishFeed() {  

      // Constructing a UserAction Object  
      var act = new gigya.services.socialize.UserAction();          
      act.setLinkBack("<?php echo sf_gigya_url_login()?>"); // Adding a Link Back        
      act.setTitle("Test Gigya"); // Setting the Title            
      act.setDescription("This is my Description"); // Setting the Description  
        
      // Parameters for the publishUserAction method,   
      var params =   
      {  
          userAction:act, // including the UserAction object  
          scope: 'both', // the Activity Feed will be published interanlly (site scope) only (not to social networks).  
          privacy: 'public',  
          callback:publishAction_callback  
      };  
        
      // Publish the User Action  
      gigya.services.socialize.publishUserAction(params);  

  }  
    
  // Display a status message according to the response from publishUserAction.  
  function publishAction_callback(response)  
  {  
      switch (response.errorCode )  
      {  
          case 0:  
              document.getElementById('status').style.color = "green";  
              document.getElementById('status').innerHTML =   
                      "Activity Feed item published, and will be presented shortly on the Activity Feed Plugin.";  
              break;  
          default:  
              document.getElementById('status').style.color = "red";  
              document.getElementById('status').innerHTML =   
                      "Unable to send Feed item. status="   
                      + response.errorCode + "; " + response.errorMessage + ";<br />"   
                      + "Please make sure you are logged in to Gigya. You may log in using the Login Plugin inside the 'Me' tab above" ;  
      }  
  }  



  // Logout from Gigya platform. This method is activated when "Logout" button is clicked   
  function logoutFromGS() {  
          gigya.services.socialize.logout(); // logout from Gigya platform        
  }  
</script>  
<div id="ActivityFeed"></div>  
  
<script type="text/javascript">  

// show the Activity Feed Plugin inside the 'ActivityFeed' <div>:  
gigya.services.socialize.showFeedUI({containerID: 'ActivityFeed'});   

</script>  
