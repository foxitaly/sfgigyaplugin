<div id="commentsDiv"></div>  
<script type="text/javascript">  
  
    // Optional - define templates to changes structures in the Comments Plugin  
    var templates= {  
        //comment: 'Hello!<br>username: $username<br>Photo Div: $photoDiv<br>Date String:$dateString<br>Body:$body<br>' +  
        //      'Replies Count: $repliesCountPlaceholder<br>Reply Button:$replyButton',  
        //commentBox: 'Login Canvas: $loginCanvas<br>Close Icon:$closeIcon<br>Add Comment:$addComment',  
        //loginCanvas_loggedIn: '$photoDiv<br>$username<br>$logoutLink',  
        //loginCanvas_loggedIn_guest: 'guest! <br>$photoDiv<br>$username<br>$logoutLink',  
        //loginCanvas_loggedOut: 'out!<br>$photoDiv<br>$loginDropdown<br>$guestDropdown'  
    }  
      
    var params =  
    {  
        // Required parameters:  
        categoryID: 'Channel',  
        containerID: 'commentsDiv',  
          
        // Optional parameters:  
        templates: templates,  
        streamTitle: 'myTitle',  
        streamID: '<?php echo (isset($streamID)) ? $streamID : $sf_request->getPathInfo();?>',  
        cid: ''  
    }  
    // Load the Comments Plugin  
    gigya.services.socialize.showCommentsUI(params);  
</script>  
