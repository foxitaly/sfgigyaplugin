<div id="followButtons"></div>
<script>
var params ={ 
  containerID: 'followButtons',
        buttons:[
    {
      provider: 'facebook',
      actionURL: 'https://www.facebook.com/gigya',
      action: 'dialog'
    },
    {
      provider: 'twitter',
      followUsers: 'gigya, gigyaDev', 
      action: 'dialog'
    },
                {
      provider: 'googleplus',
      actionURL:'https://plus.google.com/107788669599113247501/posts',
      action: 'window'
    }, 
    {
      provider: 'email',
      confirmationText:'Thank you for following us',            
      action: 'dialog'
    },
    { provider: 'rss',
      actionURL: 'http://blog.gigya.com/',
      action: 'window'
    },
    {
      provider: 'linkedin',
      actionURL: 'http://www.linkedin.com/company/gigya',
      action: 'redirect'
    }
  ]
};

gigya.services.socialize.showFollowBarUI(params);  


</script>
