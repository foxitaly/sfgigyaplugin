<SCRIPT type="text/javascript" lang="javascript" >

  var userStatusParams = {
    containerID: 'divUserStatus'
  }

  var leaderboardParams = {
    containerID: 'divLeaderboard'
  }
 
 gigya.services.gm.showUserStatusUI(userStatusParams);
 gigya.services.gm.showLeaderboardUI(leaderboardParams);


</SCRIPT>

<div id="divUser" style="display:none"></div>
<div id="divUserStatus"></div>
<div id="divAchievements"></div>
<div id="divLeaderboard"></div>
