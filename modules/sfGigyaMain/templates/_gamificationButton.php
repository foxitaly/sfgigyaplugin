<?php if(!$sf_user->isAuthenticated()): ?>
	<a href="<?php echo url_for('@sfGigyaLogin');?>" class="partecipaButton" id="gLogin">SCOPRI I VINCITORI</a>
<?php else: ?>
	<?php if(!$sf_user->hasAcceptedRules()): ?>
  	<a href="javascript:void(0)" class="accettaButton" id="cb">SCOPRI I VINCITORI</a>
	<script type="text/javascript" src="/js/foxtv/jquery.colorbox.js"></script>
  	<script type="text/javascript">
  		$('#cb').click(function(){
      		$.colorbox({
        		href: "<?php echo url_for('@sfGigyaLandingGamification'); ?>?nl=true" 
      		});
    	});
  	</script>
  	<?php else: ?>
    <a href="<?php echo url_for('@sfGigyaLandingGamification'); ?>" class="iniziaButton">SCOPRI I VINCITORI</a>
	<?php endif; ?>

<?php endif; ?>