<?php slot('staticMeta'); ?>
<!--  META GENERICI -->
<meta name="identifier-url" content="<?php echo $sf_request->getUri();?>" />
<meta name="title" content="FOX LOVERS - GIOCA E VINCI I PREMI IN PALIO" />
<meta name="description" content="Dimostra il tuo amore per le serie FOX. Gioca su Foxtv.it e vinci un tablet Microsoft e i gadget delle tue serie preferite" />
<meta name="rating" content="General" />
<meta name="revisit-after" content="1 days" />
<!--  META GENERICI -->


<!-- GENERIC OG META -->
<meta property="fb:page_id"   content="133204090065163" />
<meta property="fb:admins"    content="" />
<meta property="og:type"        content="website" />
<meta property="og:url"         content="<?php echo $sf_request->getUri(); ?>" />
<meta property="og:site_name"   content="<?php echo $sf_request->getHost(); ?>" />
<meta property="og:image"       content="http://<?php echo $sf_request->getHost(); ?>/sfGigyaPlugin/images/logo-gamification.png">
<meta property="og:title"       content="FOX LOVERS - GIOCA E VINCI I PREMI IN PALIO" />
<meta property="og:description" content="Dimostra il tuo amore per le serie FOX. Gioca su Foxtv.it e vinci un tablet Microsoft e i gadget delle tue serie preferite">
<!-- GENERIC OG META -->

<!-- LINK REL -->
<link rel="image_src" href="http://<?php echo $sf_request->getHost(); ?>/sfGigyaPlugin/images/logo-gamification.png">
<link rel="canonical" href="<?php echo $sf_request->getUri();?>" />
<?php end_slot(); ?>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div style="display:block; width:auto; height:10px; margin:0px; padding:0px;"></div>
<div id="wrapper1">
    <div id="box1">
        <h2>IL CONCORSO È TERMINATO	</h2>
        <?php /* include_partial('sfGigyaMain/gamificationButton'); */?>
    </div>
</div>

<div id="wrapper4">
	<ul class="menu">
        <li><a href="<?php echo url_for('@sfGigyaLandingGamification'); ?>" title="Come si gioca" class="<?php echo ($sf_request->getParameter('action') == 'gamification') ? "selected" : ""; ?>">I VINCITORI</a></li>
        <li><a href="<?php echo url_for("@sfGigyaGamificationRules")?>" title="Regolamento" class="<?php echo ($sf_request->getParameter('action') == 'rules') ? "selected" : ""; ?>">REGOLAMENTO</a></li>
        <li><a href="<?php echo url_for("@sfGigyaGamificationAwards")?>" title="Premi" class="<?php echo ($sf_request->getParameter('action') == 'awards') ? "selected" : ""; ?>">PREMI</a></li>
        <li><a href="<?php echo url_for("@sfGigyaGamificationHowto")?>" title="Come fare punti" class="<?php echo ($sf_request->getParameter('action') == 'howto') ? "selected" : ""; ?>">COME FARE PUNTI</a></li>
        <?php /*<li class="last"><a href="<?php echo url_for('@sfGigyaOverallRank'); ?>" title="Classifica">CLASSIFICA</a></li> */?>
        <div class="clear"></div>
  	</ul>
</div>