<script type="text/javascript" src="http://cdn.gigya.com/js/socialize.js?apiKey=<?php  echo sfConfig::get('app_gigya_apikey');?>">
{
  lang: 'it'
  ,enabledProviders: 'facebook,twitter,google,yahoo'

}
</script>

<script>

  function displayBlockUser(response)
  {
    if ( response.errorCode == 0 ) {  
      if ( null!=response.data ) {  

        if(response.data.firstName != undefined && !response.data.firstName)
        {
          $('#divUser').append('<a href="<?php echo url_for('@sfGigyaProfileUser')?>">Non hai completato la tua registrazione</a><br />');
          $('#divUser').show();
        
        }
      }
    }
  }

  

  function isLogged(responseObject){
      if (!responseObject) {
          gigya.services.socialize.getUserInfo({
              callback: isLogged,
              extraFields: "interests,activities"
          });
          return;
      }
      $.ajax({
        method: "POST"
        ,url: "<?php echo url_for('@sfGigyaVerifyIsLogged');?>"
        ,data: "UID=" + responseObject.user.UID
        ,success: function(data){
          $('#loginBox').html();
          $('#loginBox').html(data);
          $('#loginBox').show();

          if (!(responseObject.user && responseObject.user.UID)) {
            $('#divUser').append('<a href="<?php echo url_for('@sfGigyaLogin')?>">Loggati</a><br />');
            $('#divUser').show();
          }


        }
      });
  }

  function showUserInfo(responseObject){
    if (!responseObject) {
        try {
            document.getElementById('detail').value = "Getting user infos...\n";
        } 
        catch (e) {
        }
        
        // force collection of user info
        gigya.services.socialize.getUserInfo({
            callback: showUserInfo
        });
        return;
    }
      
    var userAttributes = ['UID', 'isSiteUID', 'UIDSig', 'timestamp', 'isSiteUser', 'isLoggedIn', 'isConnected', 'loginProvider', 'providers', 'photoURL', 'thumbnailURL', 'nickname', 'lastName', 'firstName', 'gender', 'age', 'birthDay', 'birthMonth', 'birthYear', 'email', 'country', 'state', 'city', 'zip', 'profileURL', 'proxiedEmail'];
    var info = "User Object contents:\n---------------------";
        
    if (responseObject.user && responseObject.user.UID) {
      // read all User attributes for display
      for (var ii in userAttributes) {
          info += "\n" + userAttributes[ii] + ": " + responseObject.user[userAttributes[ii]].toString();
      }
      
      info += "\ncapabilities";
      for (ii in responseObject.user['capabilities']) {
          info += "\n    " + ii + ': ' + responseObject.user['capabilities'][ii];
      }
      
      info += "\nidentities";
      for (ii in responseObject.user['identities']) {
          info += "\n    " + ii + ' (UID=' + responseObject.user['identities'][ii].providerUID + ') + more detail available...';
      }
    } else {
      info = "No user info available."
    }
    
    // Display info
    try {
        document.getElementById('detail').value = info + "\n";
    } 
    catch (e) {
    }
  }

  function onLogoutHandler(eventObj) {  
    gigya.services.gm.showAchievementsUI({containerID: 'divAchievements', excludeChallenges:''});
  }


  function eventHandler(eventObject){
    gigya.services.gm.showUserStatusUI(userStatusParams); 
  }
  
  
  function responseHandler(responseObject){
  }

              
  gigya.services.socialize.addEventHandlers({
    onLogin: eventHandler,
    onLogout: onLogoutHandler,
    callback: responseHandler
  });

  isLogged();
  showUserInfo();

  var params = { 
    callback: displayBlockUser
  }
  gigya.gcs.getUserData(params);


</script>
