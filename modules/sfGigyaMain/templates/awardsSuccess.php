<?php include_partial('sfGigyaMain/assetsGamification'); ?>
<?php include_partial('sfGigyaMain/headGamification'); ?> 

<div id="wrapper2">

	<h3>LISTA DEI PREMI IN PALIO PER IL CONCORSO FOX LOVERS</h3>
 	<br /><br />
    <img src="/sfGigyaPlugin/landingGamification/images/foto/tablet.jpg" class="left marginBottom10 marginRight10" />
	<p><strong>1  Tablet Microsoft 64 GB Surface RT con cover touch</strong></p>
	<p>Ad estrazione tra i primi 10 utenti in graduatoria nella Classifica Generale<br />
	*Commenta, condividi e vota i contenuti di tutto il sito  Foxtv.it per scalare la classifica di questa serie e vincere il Tablet in palio. I punti acquisiti nelle classifiche delle singole serie tv vanno a sommarsi al punteggio Totale.
    <br /><br /><a href="<?php echo url_for('@sfGigyaOverallRank'); ?>" class="button module220" style="font-size:10px">VAI ALLA CLASSIFICA GENERALE</a></p>
	
    <hr class="separator clear" />
    
    <img src="/sfGigyaPlugin/landingGamification/images/foto/ceraunavolta.jpg" class="left marginBottom10 marginRight10" />
	<p><strong>20T-Shirt originali di “C’era Una Volta”</strong></p>
	<p>Assegnati a fine concorso ai primi 20 in graduatoria nella Classifica C’era Una Volta
	<br />*Commenta, condividi e vota i contenuti di C’era Una Volta su Foxtv.it per scalare la classifica di questa serie e vincere il premio relativo
    <br /><br /><a href="<?php echo url_for('@sfGigyaOverallRank?challengeName=ceraunavolta'); ?>" class="button module220" style="font-size:10px">VAI ALLA CLASSIFICA C’ERA UNA VOLTA</a><a href="/cera-una-volta" class="button module220" style="font-size:10px">MINISITO C’ERA UNA VOLTA</a></p>
    
    <hr class="separator clear" />
    
    <img src="/sfGigyaPlugin/landingGamification/images/foto/simpson.jpg" class="left marginBottom10 marginRight10" />
    <p><strong>20 Poster “Homer Simpson"</strong></p>
    <p>Assegnati a fine concorso ai primi 20 in graduatoria nella Classifica I Simpson<br />
    *Commenta, condividi e vota i contenuti de I Simpson Foxtv.it per scalare la classifica di questa serie e vincere il premio relativo
    <br /><br /><a href="<?php echo url_for('@sfGigyaOverallRank?challengeName=simpson'); ?>" class="button module220" style="font-size:10px">VAI ALLA CLASSIFICA SIMPSON</a><a href="/i-simpson" class="button module220" style="font-size:10px">MINISITO SIMPSON</a></p>
    
    <hr class="separator clear" />
 	
    <img src="/sfGigyaPlugin/landingGamification/images/foto/walking.jpg" class="left marginBottom10 marginRight10" />
	<p><strong>10 VIDEOGIOCHI ACTIVISION “THE WALKING DEAD – SURVIVAL INSTINCT”</strong></p>
	<p>Assegnati a fine concorso ai primi 10 in graduatoria nella Classifica The Walking Dead<br />
	*Commenta, condividi e vota i contenuti di The Walking Dead su Foxtv.it per scalare la classifica di questa serie e vincere il premio relativo
	<br /><br /><a href="<?php echo url_for('@sfGigyaOverallRank?challengeName=walking'); ?>" class="button module220" style="font-size:10px">VAI ALLA CLASSIFICA THE WALKING DEAD</a><a href="/the-walking-dead" class="button module220" style="font-size:10px">MINISITO THE WALKING DEAD</a></p>
 	
    <hr class="separator clear" />
 	
    <img src="/sfGigyaPlugin/landingGamification/images/foto/glee.jpg" class="left marginBottom10 marginRight10" />
	<p><strong>10 ALBUM DIGITALI ITUNES “GLEE – THE MUSIC”</strong></p>
	<p>Assegnati a fine concorso ai primi 10 in graduatoria nella Classifica Glee<br />
	*Commenta, condividi e vota i contenuti di Glee  su Foxtv.it per scalare la classifica di questa serie e vincere il premio relativo
	<br /><br /><a href="<?php echo url_for('@sfGigyaOverallRank?challengeName=cooking'); ?>" class="button module220" style="font-size:10px">VAI ALLA CLASSIFICA GLEE</a><a href="/the-walking-dead" class="button module220" style="font-size:10px">MINISITO GLEE</a></p>
 
 	<hr class="separator clear" />
 	
    <img src="/sfGigyaPlugin/landingGamification/images/foto/davinci.jpg" class="left marginBottom10 marginRight10" />
	<p><strong>10 GADGET “DA VINCI’S DEMONS”</strong></p>
	<p>Assegnati a fine concorso ai primi 10 in graduatoria nella Classifica Da Vinci’s Demons<br />
	*Commenta, condividi e vota i contenuti di Da Vinci’s Demons su Foxtv.it per scalare la classifica di questa serie e vincere il premio relativo
	<br /><br /><a href="<?php echo url_for('@sfGigyaOverallRank?challengeName=davinci'); ?>" class="button module220" style="font-size:10px">VAI ALLA CLASSIFICA DA VINCI’S DEMONS</a><a href="/da-vincis-demons" class="button module220" style="font-size:10px">MINISITO DA VINCI’S DEMONS</a></p>
   
   	<div class="clear"></div>
    
    <br />
   
</div>