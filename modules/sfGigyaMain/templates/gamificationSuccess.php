<?php include_partial('sfGigyaMain/assetsGamification'); ?>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel();
});
</script>


<script>
function CAMBIO_IMG(path)
      {
      document.getElementById("accedi_top").src=path;	   
      }
	  

function CAMBIO_IMG_BOTTOM(path)
      {
      document.getElementById("accedi_bottom").src=path;	   
      }
</script>

<!--[if IE 6]>
<script src="/sfGigyaPlugin/landingGamification/js/DD_belatedPNG_0.0.8a.js" type="text/javascript"></script>
<script>
    DD_belatedPNG.fix('.pngBg, img.png');
</script>
<![endif]-->
<?php include_partial('sfGigyaMain/headGamification'); ?> 
 
	<div id="wrapper2">
  		<div id="contents">
     		<!--div id="partecipa_sfida_amaci">E PARTECIPA ALLA SFIDA!</div-->
     		<a id="amaci_fb" href="https://www.facebook.com/foxitalia">AMACI SU</a>
     		<div id="like_fb" class="fb-like" data-href="https://www.facebook.com/foxitalia" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
     
     		<div style="width:920px; position:absolute; top:60px; left:10px;">
            
            	<h2 style="text-align:center; font-family:'TeXGyreAdventorBold'">IL CONCORSO &Egrave; TERMINATO</h2>
                
                <h3 style="font-weight:normal; padding:20px 0px; font-family:'TeXGyreAdventorRegular'"><span style="font-family:'TeXGyreAdventorBold'">I VINCITORI DEI GADGET DELLE SINGOLE SERIE (assegnazione premi per posizione in classifica serie tv)</span> saranno contattati via mail nelle prossime settimane per la comunicazione della vincita e l’invio dei gadget</h3>
                
                <a href="/images/foxtv/classifica-foxlovers-competitions.zip" title="scarica la classifica" class="button" style="width:450px; margin:20px auto">SCARICA LE CLASSIFICHE DELLE SERIE ALLE 24.01 DEL 15 GIUGNO (*)</a>
                
                <hr />
				
                <h3 style="font-weight:normal; padding:20px 0px; font-family:'TeXGyreAdventorRegular'"><span style="font-family:'TeXGyreAdventorBold'">IL VINCITORE DEL TABLET MICROSOFT SURFACE (assegnazione tramite estrazione)</span> verrà estratto a sorte nelle prossime settimane tra i primi 10 nominativi della classifica generale (overall) alla data del 15 giugno, giorno di chiusura del concorso.</h3>
                
                <h3 style="text-align:center; font-family:'TeXGyreAdventorBold'">CLASSIFICA DEI PRIMI 10 NOMINATIVI DELLA CLASSIFICA "OVERALL" (**)</h3>
                
                <style>
					ol {width: 180px; margin:0px auto; padding:20px 0px;}
					ol li {list-style:decimal; font-size:18px; line-height:26px; font-family:'TeXGyreAdventorRegular'}
				</style>
                
                <ol>
                	<li>Francesca Corrao</li>
                    <li>James Mesfun</li>
                    <li>Andrea Durante</li>
                	<li>Carmelo Bordenga</li>
                    <li>Antonio Cagnetti</li>
                    <li>antoaccordino</li>
                    <li>Gabriele Sposetti</li>
                    <li>Elena Caravaggi</li>
                    <li>JKetti</li>
                    <li>Valentina Esposito</li>
                </ol>
                
                <hr />
                
                <h3 style="font-weight:normal; padding:20px 0px; font-family:'TeXGyreAdventorRegular'; text-align:center">
                	<span style="font-family:'TeXGyreAdventorBold'">GRAZIE A TUTTI DI AVER PARTECIPATO AL CONCORSO!</span>
                    <br />&Egrave; stata una splendida avventura, ma non finisce qui!<br /><br />
                    <span style="font-family:'TeXGyreAdventorBold'">CONTINUA A DIMOSTRARE IL TUO AMORE PER FOX!</span>
                    <br />Commenta, condividi, interagisci con contenuti del nostro sito e ottieni punti <br />per guadagnare i fantastici badge FOX!
                </h3>
                
                <hr />
                <p>(*) Le classifiche rappresentano i dati aggiornati alle 00.01 del 15 Giugno 2013 e sono da ritenersi provvisorie in quanto sono in corso le verifiche sui dati anagrafici e sulla effettiva accettazione degli utenti in classifica del regolamento del concorso.<br />Le verifiche saranno avviate a partire dal 15 Giugno 2013 e avranno termine il 18 Giugno 2013. Per quest'ultima data provvederemo a pubblicare le classifiche definitive.</p>
                <p>(**) La classifica qui riportata rappresenta i dati aggiornati alle 00.01 del 15 Giugno 2013 ed è da ritenersi provvisoria in quanto sono in corso le verifiche sui dati anagrafici e sulla effettiva accettazione degli utenti in classifica del regolamento del concorso.<br />Le verifiche saranno avviate a partire dal 15 Giugno 2013 e avranno termine il 18 Giugno 2013. Per quest'ultima data provvederemo a pubblicare la classifica definitiva.</p>
     		
            </div>
         
     <!--div id="testo_dx">
     <h2>
     REGISTRATI O ACCEDI A FOXTV.IT
     </h2>
     <p>
     Effettua la registrazione al sito o fai il login e accetta i termini del regolamento per partecipare al concorso.
     </p>
     </div>
     <div id="testo_sx">
     <h2>
     CONDIVIDI, COMMENTA E VOTA I CONTENUTI DEL SITO<br />
     PER CONQUISTARE I BADGE DELLE SERIE FOX.
     </h2>
     <p>
     Interagisci con i contenuti del sito: commenta una gallery, condividi un video o semplicemente effettua il login. Ciascuna azione ti farà guadagnare punti (per saperne di più consulta la sezione "come fare punti")
     </p>
     </div>
     <div id="testo_finale">
     <h2>
     SCALA LE CLASSIFICHE E VINCI 1 TABLET MICROSOFT SURFACE E I GADGET DELLE TUE SERIE PREFERITE.
     </h2>
     <p>
     Raggiungi la vetta della classifica generale per partecipare all'estrazione del tablet, o qualificati nelle prime posizioni delle classifiche speciali per vincere i gadget originali delle serie FOX
     </p>
     </div>
     <div id="registrati_bottom">
     	<?php include_partial('sfGigyaMain/gamificationButton'); ?>
     </div!-->
    </div>
   </div>
   
   <?php /*
   <div id="wrapper3">
       <div id="wrapper_galery">
           
        <div id="gadget_love_fox">E PER LE SERIE <font style="color:#fe7719">FOX</font> PI&Ugrave; VISTE PUOI CONQUISTARE DEI BADGE SPECIALI!</div> 
           
        <div class="jcarousel-container">
            <ul id="mycarousel" class="jcarousel-skin-tango">
            	<?php $challenges = sfConfig::get('app_gigya_challenges'); ?>
            	<?php foreach($challenges as $slugShow => $challenge): ?> 
            	<li><a href="<?php echo url_for('@show?slug=' . $slugShow);?>"><img class="png" src="/sfGigyaPlugin/landingGamification/images/<?php echo $challenge; ?>.png" width="266" height="178" alt="" /></a></li>
            	<?php endforeach; ?>
            </ul>
        </div>
          
       </div>
   </div>
   */ ?>