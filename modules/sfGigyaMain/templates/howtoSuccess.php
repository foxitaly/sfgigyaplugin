<?php include_partial('sfGigyaMain/assetsGamification'); ?>
<?php include_partial('sfGigyaMain/headGamification'); ?> 

<div id="wrapper2">

	<h3>COME FARE PUNTI</h3>
	
    <br /><br />
    
	<p>
    REGISTRAZIONE AL SITO: +50 Punti<br /><br />
	LOG-IN AL SITO: +5 Punti (max.5 al giorno)<br /><br />
    *REACTION AD UN CONTENUTO (Foto, video, news etc) :  +5 Punti<br /><br />
    *COMMENTO AD UN CONTENUTO (Foto, video, news etc): +5 Punti<br /><br />
    *CONDIVISIONE DI UN CONTENUTO (Foto, video, news etc): +10 Punti<br /><br />
    *Le azioni segnalate con asterisco possono essere effettuate fino ad un massimo di 10 volte al giorno per ciascuna competizione.
    <p>
    
    <hr />
 
	<p style="color:#666">
    <strong>RICORDA</strong>
	<br />Per accumulare i punti nelle competition delle singole serie TV e vincere il gadget relativo devi interagire con i contenuti di quella serie e conquistare i badge della serie.
	<br />Per accumulare i punti nella competition generale e partecipare all’estrazione del Primo Premio (Tablet Windows) devi interagire con i contenuti di tutto il sito.
	<br />I punti accumulati nelle competition delle serie si sommano al punteggio della competition generale.
    </p>
    
    <br /><br />

</div>
