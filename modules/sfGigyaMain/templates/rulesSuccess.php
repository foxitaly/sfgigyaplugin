<?php include_partial('sfGigyaMain/assetsGamification'); ?>
<?php include_partial('sfGigyaMain/headGamification'); ?> 

<div id="wrapper2">

	<h3>LISTA DEI PREMI IN PALIO PER IL CONCORSO FOX LOVERS</h3>
 	<br /><br />
    
    <h4>CONCORSO A PREMI "FOX LOVERS"<br />REGOLAMENTO</h4>
    
    <p>
    <strong>SOGGETTO PROMOTORE:</strong><br />
    FOX INTERNATIONAL CHANNELS ITALY S.r.l., con sede in Roma, Via Salaria n. 1021, C.F. 07448311006
    <br /><br />
    <strong>SOGGETTO DELEGATO:</strong><br />
	Amarena Company S.r.l. – con sede legale in Roma, Viale Bruno Buozzi n. 58/a, e sede operativa in Roma, Via Tagliamento n. 14, C.F. 09079361003.
    <br /><br />
	<strong>OGGETTO DELLA PROMOZIONE:</strong><br />
	Canale satellitare FOX, canale 111 di Sky.
	<strong>AREA:</strong><br />
	Territorio nazionale.
    <br /><br />
	<strong>TIPOLOGIA:</strong><br />
	Concorso di abilità e sorte.
   	<br /><br />
    </p>
    <p><a href="/sfGigyaPlugin/regolamento.pdf" title="Scarica il regolamento" class="button module300" style="font-size:12px" target="_blank">SCARICA TUTTO IL REGOLAMENTO</a></p>
    
    <div class="clear"></div>

</div>
