<?php

require_once dirname(__FILE__).'/../lib/BasesfGigyaRegisterUserActions.class.php';

/**
 * sfGigyaRegisterUser actions.
 * 
 * @package    sfGigyaPlugin
 * @subpackage sfGigyaRegisterUser
 * @author     Mauro DAlatri
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfGigyaRegisterUserActions extends BasesfGigyaRegisterUserActions
{
}
