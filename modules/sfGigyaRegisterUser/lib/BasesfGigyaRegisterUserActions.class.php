<?php

/**
 * Base actions for the sfGigyaPlugin sfGigyaRegisterUser module.
 *
 * @package     sfGigyaPlugin
 * @subpackage  sfGigyaRegisterUser
 * @author      Mauro DAlatri
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfGigyaRegisterUserActions extends sfActions
{

  /**
   * Returns the partial rendered content.
   *
   * If the vars parameter is omitted, the action's internal variables
   * will be passed, just as it would to a normal template.
   *
   * If the vars parameter is set then only those values are
   * available in the partial.
   *
   * @param  string $templateName partial name
   * @param  array  $vars         vars
   *
   * @return string The partial content
   */
  public function getPartial($templateName, $vars = null)
  {
    try
    {
      $this->getContext()->getConfiguration()->loadHelpers('CustomPartial');
    }
    catch(Exception $e)
    {
      $this->getContext()->getConfiguration()->loadHelpers('Partial');
    }

    $vars = null !== $vars ? $vars : $this->varHolder->getAll();

    return get_partial($templateName, $vars);
  }


  /**
   * preExecute
   *
   */
  public function preExecute()
  {
    $this->gigyaWrap = new GigyaWrap();
  }

 /**
   * postExecute
   *
   */
  public function postExecute()
  {
    $request = $this->getRequest();
    if($this->getUser()->hasAttribute('aGigyaRefer'))
    {
      $aGigyaReferer = $this->getUser()->getAttribute('aGigyaRefer', false);
    }
    else
    {
      $aRef = str_replace("http://" . $request->getHost(), '', $request->getReferer());
      $this->getUser()->setAttribute('aGigyaReferer',$aRef);
      $aGigyaReferer = $aRef;
    }

  }


  /**
   * redirectIfAuth
   *
   */
  private function redirectIfAuth()
  {
    $this->redirectif($this->getUser()->isAuthenticated(), '@sfGigyaSignupUser');
  }

  /**
   * Execute forgot password
   *
   */
  public function executeForgotPassword(sfWebRequest $request)
  {
    $this->form = new sfGigyaUserLostPasswordForm();
    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $UID = $this->gigyaWrap->retrieveUserByEmail($this->form->getValue('email_s'));
        $userArray['email']   = $this->form->getValue('email_s');
        $userArray['email_s'] = $this->form->getValue('email_s');
        $newPass = $this->gigyaWrap->createRandomPassword();
        $userArray['password_s'] =  $this->gigyaWrap->generateHash($newPass);

        $flag = $this->gigyaWrap->setUserProfileData($UID,json_encode($userArray));
        $this->getUser()->setFlash('flagChangePassword', $flag);
        if($flag)
        {
          $this->sendChangedPasswordMail($this->form->getValue('email_s'), $newPass);
        }
      }
    }
  }

  /**
   * execute signup
   *
   * @access public
   * @return void
   */
  public function executeSignup(sfWebRequest $request)
  {
    if ($request->isXmlHttpRequest() && $this->getUser()->isAuthenticated())
    {
      $this->getResponse()->setContentType('application/json');
      $this->getResponse()->setStatusCode(302);
      echo json_encode(array('url' => $this->generateUrl('homepage')));

      return sfView::NONE;
    }

    $this->redirectif($this->getUser()->isAuthenticated(), '@homepage');

    $referer = str_replace($request->getUriPrefix(), '', $request->getReferer());
    $this->getUser()->setAttribute('gigyaRedirectUrl' , $this->getUser()->getAttribute('gigyaRedirectUrl',$referer));

    $this->form = new sfGigyaUserSignupForm();

    if (!$request->isMethod('post'))
    {
      return sfView::SUCCESS;
    }

    $this->form->bind($request->getParameter($this->form->getName()));
    if (!$this->form->isValid())
    {
      return sfView::SUCCESS;
    }

    $UID = uniqid('SUID_' . md5(rand()));
    if (!$this->gigyaWrap->isEmailUnique($this->form->getValue('email')))
    {
      throw new Exception('Gigya Error: Already exist an user with this email');
    }

    $userInfoObj = json_encode(array('email' => $this->form->getValue('email')));
    $returnVal = $this->gigyaWrap->notifySiteLogin($UID, true, $userInfoObj ); // Notify Gigya of the site login
    if ($returnVal != "success")
    {
      throw new Exception('Gigya Error: ' . json_encode($returnVal));
    }

    // Add the new user record to the GCS.
    if($this->gigyaWrap->registerNewUser($UID,$this->form->getValue('email'),$this->form->getValue('password')))
    {
      $_data = Array('referer' => $this->getUser()->getAttribute('gigyaRedirectUrl','@homepage'));
      $this->gigyaWrap->setUserProfileData($UID,json_encode($_data));

      $sfGigyaUserProfileData = $this->gigyaWrap->getUserProfileData($UID);

      $this->sendConfirmMail($UID, $this->form->getValue('email'), $this->form->getValue('password'), $sfGigyaUserProfileData['confirmedCode']);

      if ($request->isXmlHttpRequest())
      {
        $request->setParameter('uid', $UID);
        $this->executeConfirm($request);
        $this->setTemplate('confirmMessage');

        return sfView::SUCCESS;
      }

      $this->redirect('@sfGigyaUserConfirmMessage');
    }
  }

   /**
   * execute link
   *
   * @access public
   * @return void
   */
  public function executeLinkNoMail(sfWebRequest $request)
  {
    $this->form = new sfGigyaUserLinkNoMailForm();

    $sended       = $request->getParameter('sfGigyaUserLinkNoMail'); 

    #error_log("NO MAIL: " . $this->getUser()->getAttribute('gcsLogin') . "  --> ". $sended['gcsLogin']);

    if($this->getUser()->hasAttribute('gcsLogin'))
    {
      $this->form->setDefault('gcsLogin',$this->getUser()->getAttribute('gcsLogin'));
    }

    $aGcsLogin    = $this->getUser()->getAttribute('gcsLogin',$sended['gcsLogin']); 
    $sfGigyaUser  = $this->gigyaWrap->getSocialUserInfo($aGcsLogin);

    $this->getUser()->setGigyaSecurityUser($sfGigyaUser);
    $this->getUser()->setAttribute('gcsLogin',$aGcsLogin);


    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      
      if ($this->form->isValid())
      {
        $UID = uniqid('SUID_' . md5(rand()));
        //$UID = $this->getUser()->getAttribute('gcsLogin',uniqid('SUID_' . md5(rand())));
        $email  = $this->form->getValue('email');
        if($this->gigyaWrap->isEmailUnique($email))
        {

          //if(!$this->gigyaWrap->registerNewUser($this->getUser()->getAttribute('gcsLogin'),$email,uniqid('p')))
          if(!$this->gigyaWrap->registerNewUser($aGcsLogin,$email,uniqid('p')))
          {
            $this->getUser()->setFlash('err','Gigya Error: New user Registration failed');
            #$this->redirect('@sfGigyaLinkUser');
          }
          //if(!$this->gigyaWrap->finalizeGigyaRegistration($this->getUser()->getAttribute('gcsLogin'), $UID))
          if(!$this->gigyaWrap->finalizeGigyaRegistration($aGcsLogin, $UID))
          {
            $this->getUser()->setFlash('err','Gigya Error: User finalization failed');
            #$this->redirect('@sfGigyaLinkUser');
          }

          //$this->getUser()->signin($this->getUser()->getAttribute('sfGigyaSecurityUser'));
          $userArray = Array('confirmed' => true,'channel' =>  ProjectConfiguration::getThisChannel());
          $flag = $this->gigyaWrap->setUserProfileData($UID,json_encode($userArray));


          if($flag)
          {
            $sfGigyaUser = $this->gigyaWrap->getSocialUserInfo($UID, "languages, address, phones, education, honors, publications, patents, certifications, professionalHeadline, bio, industry, specialties, work, skills, religion, politicalView, interestedIn, relationshipStatus, hometown, favorites, username, locale, verified");
            $this->getUser()->setAttribute('gcsLogin',$UID);
            $this->getUser()->signin($sfGigyaUser);
            $this->gigyaWrap->notifySiteLogin($UID,true);

            $this->getUser()->setAttribute('userMustBeCompleteData',true);
            if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
            {
              $this->redirect('@sfGigyaAcceptRulesUser');
            }
            else
            {
              $this->redirect('@sfGigyaProfileUser');
            }
            
          }
          else
          {

            $this->getUser()->setFlash('err','Gigya Error: Sorry, retry later the service is not available');
            $this->redirect('@sfGigyaLinkUser');
          }

        }
        else
        {
          $this->getUser()->setFlash('err','Gigya Error: Already exist another user with the same email');
          $this->redirect('@sfGigyaLinkUser');
        }
      }
      else
      {
        $this->getUser()->setFlash('err','Email not valid');
        $this->redirect('@sfGigyaLinkUser');
      }
   
    } 

  }

  /**
   * execute link
   *
   * @access public
   * @return void
   */
  public function executeLink(sfWebRequest $request)
  {

    $sended       = $request->getParameter('sfGigyaUserLink'); 

    #error_log($this->getUser()->getAttribute('gcsLogin') . "  --> ". $sended['gcsLogin']);

    $aGcsLogin    = $this->getUser()->getAttribute('gcsLogin',$sended['gcsLogin']);
    $sfGigyaUser  = $this->gigyaWrap->getSocialUserInfo($aGcsLogin);

    $this->getUser()->setGigyaSecurityUser($sfGigyaUser);
    $this->getUser()->setAttribute('gcsLogin',$aGcsLogin);

    $this->form       = new sfGigyaUserLinkForm();
    $this->form->setDefault('gcsLogin',$aGcsLogin);
    $this->formNoMail = new sfGigyaUserLinkNoMailForm();
    $this->formNoMail->setDefault('gcsLogin',$aGcsLogin);

    $UID        = uniqid('SUID_' . md5(rand()));
    if($this->getUser()->hasAttribute('gcsLogin'))
    {
      $this->form->setDefault('gcsLogin',$this->getUser()->getAttribute('gcsLogin'));
    }


    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));

      if ($this->form->isValid())
      {
        $UID = $this->gigyaWrap->userGCSLogin($this->form->getValue('email'), $this->form->getValue('password_s'));
        if($UID != -1)
        {
          $sfGigyaUserProfileData = $this->gigyaWrap->getUserProfileData($UID);
          $sfGigyaUser            = $this->gigyaWrap->getSocialUserInfo($UID);


          if(!$sfGigyaUserProfileData['confirmed']) $this->redirect('@sfGigyaUserMustBeConfirmed');

          //$this->gigyaWrap->finalizeGigyaRegistration($this->getUser()->getAttribute('gcsLogin'), $UID);
          $this->gigyaWrap->finalizeGigyaRegistration($aGcsLogin, $UID);
          $this->getUser()->setAttribute('gcsLogin',$UID);
        
          $this->getUser()->signin($sfGigyaUser);
          $this->gigyaWrap->notifySiteLogin($UID,true);

/* 

          $aRef = (isset($sfGigyaUserProfileData['referer'])) ? $sfGigyaUserProfileData['referer'] : $this->getUser()->getAttribute('gigyaRedirectUrl', '@homepage');

          //if($aRef == '@homepage') $this->redirect($aRef);
          
          if(strstr($aRef,"@"))
          {
            $this->redirect($aRef);
          }
          else
          {
            $this->redirect("http://" . $request->getHost() . $aRef);
          }
*/
          if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
          {
            $this->getUser()->setAttribute('fromLinking',true);
            $this->redirect('@sfGigyaAcceptRulesUser');
          }
          else
          {
            $aRef = (isset($sfGigyaUserProfileData['referer'])) ? $sfGigyaUserProfileData['referer'] : $this->getUser()->getAttribute('gigyaRedirectUrl', '@homepage');
            
            if(strstr($aRef,"@"))
            {
              $this->redirect($aRef);
            }
            else
            {
              $this->redirect("http://" . $request->getHost() . $aRef);
            }

          }


        }
        else
        {
          $this->getUser()->setFlash('err','Gigya Error: User Not found');
          $this->redirect('@sfGigyaLinkUser');
        }

      }
    }
  }

  /**
   * execute confirm
   *
   * @params sfWebRequest $request
   *
   */
  public function executeConfirm(sfWebRequest $request)
  {

    $sfGigyaUser            = $this->gigyaWrap->getSocialUserInfo($request->getParameter('uid'));
    $sfGigyaUserProfileData = $this->gigyaWrap->getUserProfileData($sfGigyaUser->UID);

    $email        = $sfGigyaUserProfileData['email_s'];
    $confirmCode  = $sfGigyaUserProfileData['confirmedCode'];

    if($sfGigyaUser)
    {
      $UID = $sfGigyaUser->UID;
      $userInfoObj = json_encode(array('email' => $email));
      $returnVal = $this->gigyaWrap->notifySiteLogin($UID, false, $userInfoObj ); // Notify Gigya of the site login

      if($returnVal)
      {
        $this->form = new sfGigyaUserConfirmRegistrationForm(array(), array(), false);

        if ($request->isMethod('post'))
        {
          $this->form->bind($request->getParameter($this->form->getName()));
          if ($this->form->isValid())
          {
            if($this->form->getValue('code') == $confirmCode)
            {
              $userArray = Array('confirmed' => true,'email' => $email);

              $flag = $this->gigyaWrap->setUserProfileData($UID,json_encode($userArray));

              if($flag)
              {
                $this->getUser()->setGigyaUserProfileData($this->gigyaWrap->getUserProfileData($UID));
                $this
                    ->getContext()
                    ->getEventDispatcher()
                    ->notify(new sfEvent($this->getUser(), 'user_profile.updated'));

                $this->getUser()->signin($sfGigyaUser);
                $this->getUser()->setAttribute('fromSignup', true);
                $this->getUser()->setAttribute('gcsLogin',$UID);

                # DEPRECATED
                # ----------
                # $this->getUser()->setAttribute('gigyaRedirectUrl',$sfGigyaUserProfileData['referer']);
                # ----------

                $this->sendConfirmedMail($email);

                if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
                {
                  $this->redirect('@sfGigyaAcceptRulesUser');
                }
                else
                {
                  $this->redirect('@sfGigyaProfileUser');
                }
              }
            }
            else
            {
              $this->getUser()->setFlash('errorCode',true);
              $this->redirect('@sfGigyaConfirmRegistration?uid=' . $UID);
            }
          }
        }
      }
      else
      {

        $this->redirect('@homepage');
        return sfView::NONE;
      }
    }
    else
    {

      $this->redirect('@homepage');
      return sfView::NONE;
    }

  }

  /**
   * execute mustBeConfirmed
   *
   * @params sfWebRequest $request
   *
   */
  public function executeMustBeConfirmed(sfWebRequest $request)
  {
    $this->gigyaWrap->deleteUser($request->getParameter('UID',$this->getUser()->getUID()));
    $this->getUser()->signout();
  }

  /**
   * execute confirmMessage
   *
   * @params sfWebRequest $request
   *
   */
  public function executeConfirmMessage(sfWebRequest $request)
  {
    $this->gigyaWrap->notifySiteLogout(sfConfig::get('app_gigya_apikey'),sfConfig::get('app_gigya_secretkey'), $this->getUser()->getUID());
    $this->getUser()->signout();
  }
  /**
   * execute userForgotPassword
   *
   * @access public
   * @return void
   */
  public function executeUserForgotPassword(sfWebRequest $request)
  {
    $this->redirectIfAuth();
  }


  /**
   * send confirm mail to new user
   * @param $UID
   * @param $email
   * @param $password
   * @param $confirmCode
   */
  private function sendConfirmMail($UID, $email, $password, $confirmCode)
  {
    $mailer     = $this->getMailer();
    $fromMail   = sfConfig::get('app_mail_from_mail', 'noreply@foxtv.it');
    $fromName   = sfConfig::get('app_mail_from_name', 'FOX');
    $subject    = sfConfig::get('app_mail_subject_confirm_registration_request',"FOX - Conferma la tua registrazione");
    $text       = $this->getPartial('mail_confirm', array('UID' => $UID, 'email' => $email, 'password' => $password,'confirmCode' => $confirmCode));
    $message    = $mailer->compose();
    $message->setSubject($subject);
    $message->setFrom(array($fromMail => $fromName));
    $message->setTo($email);
    $message->setBody($text, 'text/html');
    $mailer->send($message);
  }

  /**
   * send confirmed mail to new user
   */
  private function sendConfirmedMail($email)
  {
    $mailer     = $this->getMailer();
    $fromMail   = sfConfig::get('app_mail_from_mail', 'noreply@foxtv.it');
    $fromName   = sfConfig::get('app_mail_from_name', 'FOX');
    $subject    = sfConfig::get('app_mail_subject_confirm_registration',"FOX - Grazie per aver confermato il tuo account");
    $text       = $this->getPartial('mail_confirmed');
    $message    = $mailer->compose();
    $message->setSubject($subject);
    $message->setFrom(array($fromMail => $fromName));
    $message->setTo($email);
    $message->setBody($text, 'text/html');
    $mailer->send($message);
  }

  /**
   * send confirmed mail to new user
   */
  private function sendChangedPasswordMail($email, $password)
  {
    $mailer     = $this->getMailer();
    $fromMail   = sfConfig::get('app_mail_from_mail', 'noreply@foxtv.it');
    $fromName   = sfConfig::get('app_mail_from_name', 'FOX');
    $subject    = sfConfig::get('app_mail_subject_password_change',"FOX - La tua password è stata cambiata");
    $text       = $this->getPartial('mail_password_changed', Array('username' => $email, 'password' => $password));
    $message    = $mailer->compose();
    $message->setSubject($subject);
    $message->setFrom(array($fromMail => $fromName));
    $message->setTo($email);
    $message->setBody($text, 'text/html');
    $mailer->send($message);
  }
}
