
<?php use_stylesheet('/sfGigyaPlugin/css/register.css')?>

<?php $action = (isset($formAction)) ? $formAction : sf_gigya_url_user_register();?>

<form class="formContainer" id="updateUserDataForm" name="updateUserDataForm" method="post" action="<?php echo $action; ?>">
  <div id="container">

    <?php echo $form->renderGlobalErrors(); ?>


    <?php if(isset($form['password_s'])): ?>
    <div>
      <p>
        <label for="firstname">*Password:</label>
        <?php echo $form['password_s']->render(Array('class' => errorClass($form['password_s']->hasError()))); ?>
      </p>
    </div>
    <?php endif; ?> 


    <div id="side-a">

      <p>
        <label for="firstname">*First Name:</label>
        <?php echo $form['firstName']->render(Array('class' => errorClass($form['firstName']->hasError()))); ?>
      </p>
      <p>
        <label for="lastname">*Last Name:</label>
        <?php echo $form['lastName']->render(Array('class' => errorClass($form['lastName']->hasError()))); ?>
      <p>
        <input type="checkbox" name="termagree" id="termagree" value="" /><label for="termagree"> I have read, understood...</label>
      </p>
      <p>
        <input type='submit' value='Invia'>
      </p>
    </div>
    <div id="orseperator">&nbsp;</div>
    <div id="side-b">
      <p>
        <label for="mobileNum">Mobile phone number:</label>
        <?php echo $form['mobileNum']->render(); ?>
      </p>
      <p>
        <label for="city">*City:</label>
        <?php echo $form['city']->render(); ?>
      </p>
      <p>
        <?php echo $form['newsletter']->render(); ?>
        <label for="newsletter">Subscribe to our newsletter</label>
      </p>
    </div>
  </div>
<?php echo $form->renderHiddenFields(); ?>
</form>

