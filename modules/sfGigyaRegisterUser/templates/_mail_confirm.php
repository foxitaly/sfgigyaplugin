Ciao 

<p>Abbiamo ricevuto una richiesta di registrazione al nostro sito con questo indirizzo mail, se sei stato tu conferma adesso la registrazione <a href="<?php echo url_for('@sfGigyaConfirmRegistration?uid=' . $UID, Array('absolute' => true)); ?>">cliccando qui</a> altrimenti ignora pure questo messaggio e i dati verranno cancellati automaticamente.</p>

<p>Questi sono i dati inseriti: </p>
<ul>
  <li>Username: <?php echo $email; ?></li>
  <li>Password: <?php echo $password; ?></li>
</ul>

<p style="color:green">Questo è il codice di conferma da inserire nella form che ti verr&agrave; presentata: <?php echo $confirmCode?></p>

<p>Grazie, la redazione di FOX</p>
