<p>Abbiamo ricevuto una richiesta di cambio password per il tuo account. La nuova password che hai ricevuto <strong>non è stata inserita da richiedente </strong>, ma generata automaticamente dai nostri sistemi.</p>
<br /><br />
<p>Eccoti il riepilogo dei tuoi dati di accesso:</p>

<br /><br />
<p>
<strong>Username: </strong> <?php echo $username?><br />
<strong>Password: </strong> <?php echo $password; ?>
</p>


