<?php if($sf_user->hasFlash('errorCode')): ?>

<div class="gigyaError">
  <h3>ATTENZIONE!!! Il codice inserito risulta errato</h3>
</div>

<?php endif; ?>

<form method="post" action="<?php echo url_for('@sfGigyaConfirmRegistration?uid='. $sf_request->getParameter('uid')); ?>">
  <p>Inserisci il codice ricevuto per mail</p> 
  <p><?php echo $form['code']; ?></p>
  <p><input type="submit" value="Invia"></p>
  <?php echo $form->renderHiddenFields();?>
</form>

