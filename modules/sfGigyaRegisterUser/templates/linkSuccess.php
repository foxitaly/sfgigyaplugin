<div class="span-24">

  <?php if($sf_user->hasFlash('msg')): ?>

  <div class="error"><?php echo $sf_user->getFlash('msg');?></div>

  <?php endif; ?>
  
  <?php if($sf_user->hasFlash('err')): ?>

  <div class="error"><?php echo $sf_user->getFlash('err');?></div>

  <?php endif; ?>

  <h2>Benvenuto <?php echo $sf_user->getFirstName()?></h2>

  <div class="span-12">

    <div class=" append-bottom notice block">
      <strong>NON SONO ISCRITTO AL SITO</strong><br>
      <em>Esiste gia' un utente con questa email, oppure non siamo in grado di verificare la mail nel social da te scelto, peer favore inserisci una mail valida</em>
    </div>

    <form method="post" action="<?php echo url_for('@sfGigyaLinkNoMailUser')?>">
    <table>
    <?php echo $formNoMail; ?>
    <tr>
      <td colspan="2">
        <input type="submit" id='btn1'>
      </td>
    </tr>
    </table>
    <?php echo $formNoMail->renderHiddenFields(); ?>
    </form>
  </div>
  

  <div class="span-12 last">

    <div class="append-bottom notice block">
      <strong>SONO GIA' ISCRITTO AL SITO</strong><br>
      <em>Inserisci le tue credenziali e collega il tuo account. <br /><br />Se non ricordi la password <a href="<?php echo url_for('@sfGigyaForgotPasswordUser')?>">Clicca qui</a></em>
    </div>

    <form method="post" action="<?php echo url_for('@sfGigyaLinkUser')?>">
    <table>
    <?php echo $form; ?>
    <tr>
      <td colspan="2">
        <input type="submit" id='btn2'>
      </td>
    </tr>
    </table>
    <?php echo $form->renderHiddenFields(); ?>
    </form>
  </div>

</div>
