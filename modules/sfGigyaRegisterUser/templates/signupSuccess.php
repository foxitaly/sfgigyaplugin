<?php use_stylesheet('/sfGigyaPlugin/css/register.css')?>


<script>
  $(document).ready(function() {
    // Present Gigya's Login Plugin
    gigya.services.socialize.showLoginUI({ 
      containerID: 'loginPluginDiv' // The plugin is presented in the 'loginPluginDiv' <div> element (see definition in login.php and register.php)
      ,height: 100
      ,width: 313
      ,showTermsLink: false
      ,hideGigyaLink: true
      ,showWhatsThis: false
      ,UIConfig: '<config><body><controls><snbuttons buttonsize="35" /></controls></body></config>'
      ,buttonsStyle: 'fullLogo'
      ,extraFields:'likes'
      ,facebookExtraPermissions:'user_likes'
      ,redirectURL: "<?php echo url_for('@sfGigyaVerify'); ?>"
    });
  });
</script>

<div class="section" id="page">
  <div class="contentRegister" style="height:415px;" >
    <div id="inheader">&nbsp;</div>
    <div id="container">
    

      <?php if($sf_user->hasFlash('registered')): ?>

      <h2>Scarica la posta e conferma la tua registrazione</h2>

      <?php endif; ?>

      <?php if($form->hasErrors()): ?>

        <h3>I dati inseriti risultano errati, questi gli errori: </h3>

        <?php if($form['email']->hasError()): ?>
        <h5>
        <?php echo ($form['email']->getError() == 'exist') ? "la mail inserita risulta gi&agrave; assegnata" : "L'indirizzo email inserito non &egrave; corretto (Es.: miamail@dominio.it)"; ?>
        </h5>
        <?php endif; ?>

        <?php if($form['password']->hasError()): ?>
        <h5>
          La password inserita ha una lunghezza inferiore a 8 caratteri o le due password inserite non coincidono
        </h5>
        <?php endif; ?>
        

      <?php endif; ?>

      <div id="side-a">
    

        <span id="loginTitle">Inserisci i tuoi dati:</span>
        <form class="formContainer" id="formRegister" name="formRegister" method="post" action="<?php echo sf_gigya_url_user_signup()?>">
          <p>
            <?php echo $form['email']->renderLabel();?>
            <?php echo $form['email']->render(Array('id'=>'email','class' => errorClass($form['email']->hasError())));?>
          </p>
          <p>
            <?php echo $form['password']->renderLabel();?>
            <?php echo $form['password']->render(Array('id'=>'password', 'class' => errorClass($form['password']->hasError())))?>
          </p>
          <p>
            <?php echo $form['repassword']->renderLabel();?>
            <?php echo $form['repassword']->render(Array('id'=>'repassword', 'class' => errorClass($form['repassword']->hasError())))?>
          </p>
          <!--div id="register" name="register" class="cssSpriteRegister"></div-->
          <input type='submit' value='Registrami'>
          <?php echo $form->renderHiddenFields(); ?>
        </form>
        <div id="registrationMessage" class="error">&nbsp;</div>
      </div>
      <div id="orseperator"><img src="/sfGigyaPlugin/images/or.jpg" /></div>
      <div id="side-b">
        <span id="loginTitle" style="padding-left:65px;">Registrati tramite la tua identit&agrave; sociale:</span>
        <div id="loginPluginDiv">Sto caricando ...</div>
      </div>
    </div>
    <div id="footer">
      <div class="linkback">Non sei ancora registrato? <a href="<?php echo sf_gigya_url_user_signup();?>">Che aspetti</a></div>
    </div>
  </div>
</div>

