<?php

require_once dirname(__FILE__).'/../lib/BasesfGigyaUserActions.class.php';

/**
 * sfGigyaUser actions.
 * 
 * @package    sfGigyaPlugin
 * @subpackage sfGigyaUser
 * @author     Mauro DAlatri
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfGigyaUserActions extends BasesfGigyaUserActions
{
}
