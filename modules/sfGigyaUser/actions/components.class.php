<?php

require_once dirname(__FILE__).'/../lib/BasesfGigyaUserComponents.class.php';

/**
 * sfGigyaUser components.
 * 
 * @package    sfGigyaPlugin
 * @subpackage sfGigyaMain
 * @author     Mauro DAlatri
 */
class sfGigyaUserComponents extends BasesfGigyaUserComponents
{
}
