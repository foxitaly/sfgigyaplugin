<?php

/**
 * Base actions for the sfGigyaPlugin sfGigyaUser module.
 *
 * @package     sfGigyaPlugin
 * @subpackage  sfGigyaUser
 * @author      Mauro DAlatri
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfGigyaUserActions extends sfActions
{




  /**
   * Returns the partial rendered content.
   *
   * If the vars parameter is omitted, the action's internal variables
   * will be passed, just as it would to a normal template.
   *
   * If the vars parameter is set then only those values are
   * available in the partial.
   *
   * @param  string $templateName partial name
   * @param  array  $vars         vars
   *
   * @return string The partial content
   */
  public function getPartial($templateName, $vars = null)
  {
    try
    {
      $this->getContext()->getConfiguration()->loadHelpers('CustomPartial');
    }
    catch(Exception $e)
    {
      $this->getContext()->getConfiguration()->loadHelpers('Partial');
    }

    $vars = null !== $vars ? $vars : $this->varHolder->getAll();

    return get_partial($templateName, $vars);
  }


  /**
   * preExecute
   *
   */
  public function preExecute()
  {
    $this->gigyaWrap = new GigyaWrap();
  }


  /**
   * execute acceptRules
   *
   */
  public function executeAcceptRules(sfWebRequest $request)
  {


    if(!in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
    {
      $this->forward404();
    }

    $this->form = new sfGigyaUserAcceptRulesForm;
    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));

      if ($this->form->isValid())
      {

        // Register User choice on GCS
        $userProfileData = array(); 
        $userProfileData['acceptedRules']  = $this->form->getValue('rules');
        $flag = $this->gigyaWrap->setUserProfileData($this->getUser()->getUID(), json_encode($userProfileData), true);
        if($flag)
        {
          $gigyaUser = $this->getUser()->persistMyGigyaUID();
          $gigyaUser->setRules($this->form->getValue('rules'));
          $gigyaUser->setLastLogin(date('Y-m-d'));
          $gigyaUser->save();
        }

        if($this->getUser()->getAttribute('fromLinking',false))
        {
          $this->getUser()->getAttributeHolder()->remove('fromLinking');

          $aRef = $this->getUser()->getAttribute('gigyaRedirectUrl', '@homepage');

          if(strstr($aRef,"@"))
          {
            $this->redirect($aRef);
          }
          else
          {
            $this->redirect("http://" . $request->getHost() . $aRef);
          }
        }
        else
        {
          $this->redirect('@sfGigyaProfileUser');
        }
      }
    }

    if($request->getParameter('wl',false)) $this->setLayout(false);
  }


  /**
   * execute gamification
   *
  public function executeGamification(sfWebRequest $request)
  {
    if(basename($request->getReferer()) == 'signup') $this->redirect('@homepage');
  }
   */


  /**
   * Mostra il Rank di uno user
   *
   * @param sfWebRequest $request
   */
  public function executeMyGamification(sfWebRequest $request)
  {
    $this->aUser          = $this->gigyaWrap->getUserProfileData($request->getParameter('guid',null));
    $this->myGamification = $this->gigyaWrap->getUserGamification($request->getParameter('guid',null));
    $this->challenges     = array();
    $challenges           = $this->gigyaWrap->getChallenge("*");
    foreach($challenges as $challenge)
    {
      $this->challenges[$challenge['challengeID']] = $challenge;
    }

  }

  /**
   * Mostra la classfica legata alla Gamification
   *
   * @param sfWebRequest $request
   */
  public function executeOverall(sfWebRequest $request)
  {
    $aChallenge             = $request->getParameter('challengeName','_default');
    $this->users            = $this->gigyaWrap->getTopUsers($aChallenge,$this->getUser()->getUID());
    $this->challenges       = $this->gigyaWrap->getChallenge("*"); 

    $this->currentChallenge = null;
    foreach($this->challenges as $challenge)
    {
      if ($challenge['challengeID'] == $aChallenge) $this->currentChallenge = $challenge;
    } 
  }


  /**
   * Cerca di effettuare il login con le credenziali passate in post
   *
   * @param sfWebRequest $request
   */
  public function executeLoginCheck(sfWebRequest $request)
  {
    if (!$request->isMethod('post'))
    {
        $this->getResponse()->setStatusCode('400');
        return sfView::SUCCESS;
    }

    $this->setLayout(false);

    $gWrap        = new GigyaWrap();
    $this->form   = new sfGigyaUserLoginForm();

    $this->form->bind($request->getParameter($this->form->getName()));
    if (!$this->form->isValid())
    {
        $this->getResponse()->setStatusCode('400');
        return sfView::SUCCESS;
    }

    $UID = $gWrap->userGCSLogin($this->form->getValue('email'), $this->form->getValue('password'));
    if ($UID == -1)
    {
        $this->getResponse()->setStatusCode('400');
        return sfView::SUCCESS;
    }

    $sfGigyaUser = $this->getUser()->validateSignature($UID);
    if (!$sfGigyaUser)
    {
        $this->getResponse()->setStatusCode('400');
        return sfView::SUCCESS;
    }

    $this->getUser()->setGigyaSecurityUser($sfGigyaUser);

    if (!$this->getUser()->isConfirmed($UID))
    {
        $this->getResponse()->setStatusCode('400');
        $this->message = 'L\'utente non è stato confermato';
        return sfView::SUCCESS;
    }

    $this->getUser()->setAttribute('gcsLogin',$UID);

    if (!$this->getUser()->isSiteUID())
    {
        $this->getResponse()->setStatusCode('400');
        $this->message = 'L\'utente non è riconosciuto sul sistema';
        return sfView::SUCCESS;
    }

    $this->getUser()->signin($sfGigyaUser);
    $gWrap->notifySiteLogin($UID);

    if ($this->getUser()->getFirstName() != '' && $this->getUser()->getLastName())
    {
        $this->getResponse()->setContentType('application/json');
        echo json_encode(array('url' => $this->form->getValue('ref'))); #Pagina Corrente
        return sfView::NONE;
    }


    $this->getUser()->setAttribute('userMustBeCompleteData', true);
    $this->getResponse()->setContentType('application/json');
    echo json_encode(array('url' => $this->generateUrl('sfGigyaProfileUser'))); #Pagina Corrente
    return sfView::NONE;
  }

  /**
   * execute Login
   *
   */
  public function executeLogin(sfWebRequest $request)
  {
    if ($request->isXmlHttpRequest() && $request->isMethod('post'))
    {
      $this->setTemplate('loginCheck');
      return $this->executeLoginCheck($request);
    }

    $aUri     = str_replace($request->getUriPrefix(), '', $request->getUri());
    $aReferer = str_replace($request->getUriPrefix(), '', $request->getReferer());

    $referer = ($aUri == "/socialize/login" || $aUri == '/index.php/socialize/login') ? $aReferer : $aUri ; 

    $this->getUser()->setAttribute('gigyaRedirectUrl' , $this->getUser()->getAttribute('gigyaRedirectUrl',$referer));

    if($this->getUser()->isAuthenticated()) $this->userRedirect();

    $this->form   = new sfGigyaUserLoginForm(null, Array('ref'=> $this->getUser()->getAttribute('gigyaRedirectUrl' , "@homepage")));

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $UID = $this->gigyaWrap->userGCSLogin($this->form->getValue('email'), $this->form->getValue('password'));
        if($UID != -1)
        {
          if($sfGigyaUser = $this->getUser()->validateSignature($UID))
          {

            $this->gigyaWrap->setUserProfileData($UID, json_encode(Array('referer'=> $this->getUser()->getAttribute('gigyaRedirectUrl' , "@homepage"))));
        
            $this->getUser()->setGigyaSecurityUser($sfGigyaUser);

            if(!$this->getUser()->isConfirmed($UID)) $this->redirect('@sfGigyaLogin');

            $this->getUser()->setAttribute('gcsLogin',$UID);

            if($this->getUser()->isSiteUID()){
              $REFERER = $this->getUser()->getAttribute('gigyaRedirectUrl' , "@homepage"); //Misc::buildRefererForGigyaLoginRedirect($request);
              $this->getUser()->signin($sfGigyaUser);
              $this->gigyaWrap->notifySiteLogin($UID);
              if($this->getUser()->getFirstName() != '' && $this->getUser()->getLastName()) $this->userRedirect($REFERER);
              $this->getUser()->setAttribute('userMustBeCompleteData', true);
              $this->redirect('@sfGigyaProfileUser');
            }

            $this->redirect('@sfGigyaSignupUser');

          }

        }
      }
    }


  }


  /**
   * execute verifyUserIsLogged
   *
   * @params sfWebRequest $request
   */
  public function executeVerifyIsLogged(sfWebRequest $request)
  {

/*
    $UID            = $request->getParameter("UID",null);
    $this->getUser()->verifySSO($UID);

    $this->verified = $this->getUser()->isSiteUID();
    
    if(!$this->verified) $this->getUser()->setAuthenticated(false);
*/

    $UID        = $request->getParameter("UID",null);

    if(!$UID)
    {
       $this->getUser()->signout();
		   $this->verified = false;
    }
    else
    {

      $apiKey     = sfConfig::get('app_gigya_apikey');
      $secretKey  = sfConfig::get('app_gigya_secretkey');

      $this->verified = $this->getUser()->verifySSO($UID);


      if(!$this->getUser()->getAttribute('fromSignup',false))
      {
        if(!$this->getUser()->isConfirmed($UID))
        {
          $this->setGoodBay($apiKey, $secretKey, $UID);
        }
      }

			if(!$this->verified)
			{
          $this->setGoodBay($apiKey, $secretKey, $UID);
			}

    }
    return $this->setLayout(false);
  }


  /**
   * setGoodBay
   *
   * @params string $apikey
   * @params string $secretKey
   * @params string $UID
   */
  public function setGoodBay($apiKey, $secretKey, $UID)
  {
		$this->gigyaWrap->notifySiteLogout($apiKey, $secretKey, $UID);
		$this->verified = false;
		$this->getUser()->signout();

	}


  /**
   * execute Verify
   *
   * @params sfWebRequest $request
   */
  public function executeVerify(sfWebRequest $request)
  {
    $UID      = $request->getParameter("UID",null);

    if($sfGigyaUser = $this->getUser()->validateSignature($UID))
    {
      $aGigyaUser = $this->gigyaWrap->getUserProfileData($UID);

      $ref        = ($request->getReferer()) ? str_replace("http://" . $request->getHost(), "",$request->getReferer()) : '@homepage';
      $_REFERER   = ($this->getUser()->getAttribute('gigyaRedirectUrl')) ? $this->getUser()->getAttribute('gigyaRedirectUrl') : $ref;
      
      $REFERER   = ($request->getParameter('ref')) ? $request->getParameter('ref') : $_REFERER;
     
      if(strstr($REFERER,'/socialize/verify'))
      {
        $REFERER = '@homepage';
      }

      if(strstr($REFERER,'/socialize/gamification'))
      {
        $REFERER = '@sfGigyaMyGamification?guid=' . $UID;
      }
      

      $this->getUser()->setGigyaSecurityUser($sfGigyaUser);
      $this->getUser()->setAttribute('gcsLogin',$UID);
      $this->getUser()->setAttribute('gigyaRedirectUrl',$REFERER); 


      $this->gigyaWrap->setUserProfileData($UID, json_encode(Array('referer'=> $REFERER)));

      $data = $this->gigyaWrap->getUserProfileData($UID);

      if($this->getUser()->isSiteUID())
      {
        #XXX
        if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
        {
          $this->getUser()->persistMyGigyaUID();
        }

        $this->getUser()->signin($sfGigyaUser);
        $this->gigyaWrap->notifySiteLogin($UID);
      
        # If User already confirm account or the social registratio is completed redirect to $REFERER
        if($this->getUser()->getFirstName() != '' && $this->getUser()->getLastName() != '') $this->userRedirect($REFERER);

        if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
        {
          $this->redirect('@sfGigyaAcceptRulesUser');
        }
        else
        {
          $this->redirect('@sfGigyaProfileUser');
        }
      }
      elseif ( $this->getUser()->getEmail() == '' || !$this->gigyaWrap->isEmailUnique($this->getUser()->getEmail()))
      {
        #XXX
        if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
        {
          $this->getUser()->persistMyGigyaUID();
        }
        $this->redirect('@sfGigyaLinkUser');

      }
      elseif (!$this->gigyaWrap->registerNewUser($this->getUser()->getUID(),$this->getUser()->getEmail(),uniqid('p')))
      {
        $this->getUser()->setFlash('err','Gigya Error: User not registered successfully');
        $this->redirect('@sfGigyaLogin');
      }
      else
      {
        $UID = uniqid('SUID_' . md5(rand()));
        if(!$this->gigyaWrap->finalizeGigyaRegistration($this->getUser()->getUID(), $UID))
        {
          $this->getUser()->setFlash('err','Gigya Error: The finalization of this user is not completed successfully');
          $this->redirect('@sfGigyaLogin');

        }
        else
        {
          $sfGigyaUser = $this->gigyaWrap->getSocialUserInfo($UID, "languages, address, phones, education, honors, publications, patents, certifications, professionalHeadline, bio, industry, specialties, work, skills, religion, politicalView, interestedIn, relationshipStatus, hometown, favorites, username, locale, verified");

          $userArray = Array('confirmed' => true,'channel' =>  ProjectConfiguration::getThisChannel());
          $flag = $this->gigyaWrap->setUserProfileData($UID,json_encode($userArray));


          $this->getUser()->setAttribute('gcsLogin',$UID);
          $this->getUser()->signin($sfGigyaUser);
          $this->gigyaWrap->notifySiteLogin($this->getUser()->getUID());
          $this->getUser()->setAttribute('userMustBeCompleteData',true);
          if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
          {
            $this->redirect('@sfGigyaAcceptRulesUser');
          }
          else
          {
            #XXX
            if(in_array($this->getContext()->getConfiguration()->getApplication(), sfConfig::get('app_accept_rules_appname',array('foxtv'))))
            {
              $this->getUser()->persistMyGigyaUID();
            }
            $this->redirect('@sfGigyaProfileUser');
          }
        }
      }
    }
    $this->redirect('@sfGigyaLogin');
  }

  /**
   * execute Logout
   *
   * @params sfWebRequest $request
   */
  public function executeLogout(sfWebRequest $request)
  {
    $this->getUser()->signout();
    $this->getUser()->getAttributeHolder()->clear();

    $this->redirect('@homepage');
  }


  /**
   * delete
   *
   * @params sfWebRequest $request
   *
   */
  public function executeDelete(sfWebRequest $request)
  {
    $a = $this->gigyaWrap->deleteUser($this->getUser()->getUID());
    $this->getUser()->signout();
    $this->getUser()->getAttributeHolder()->clear();
    $this->redirect('@homepage');
  }

  /**
   * profile
   *
   * @params sfWebRequest $request
   *
   */
  public function executeProfile(sfWebRequest $request)
  {


    $this->form         = new sfGigyaUserProfileForm(Array(), Array('sfUser'=>$this->getUser()));
    $this->formPassword = new sfGigyaUserChangePasswordForm();

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $userArray = $this->form->getValues();

        $userProfileData = $this->gigyaWrap->getUserProfileData($this->getUser()->getUID());
        $userArray['email']   = $userProfileData['email_s'];
        $userArray['email_s'] = $userProfileData['email_s'];
        $userArray['referer']  = $this->getUser()->getAttribute('gigyaRedirectUrl','@homepage');

        $flag = $this->gigyaWrap->setUserProfileData($this->getUser()->getUID(), json_encode($userArray));
        
        if($flag)
        {
          $this->getUser()->setGigyaUserProfileData(array_merge($userProfileData, $userArray));
          $this
            ->getContext()
            ->getEventDispatcher()
            ->notify(new sfEvent($this->getUser(), 'user_profile.updated'));
        }

        $this->getUser()->setFlash('flagProfile',$flag);

        if($this->getUser()->getAttribute('fromSignup',false) && $flag)
        {
          $this->getUser()->getAttributeHolder()->remove('fromSignup');
          $this->sendEndedRegistrationMail($userProfileData['email_s']);
          //$this->redirect('@homepage');
          $this->userRedirect('', $this->getUser()->getUID());
        }

        if($this->getUser()->getAttribute('userMustBeCompleteData',false) && $flag)
        {


          if($this->getUser()->getFirstName() && $this->getUser()->getLastName())
          {
            $this->getUser()->getAttributeHolder()->remove('userMustBeCompleteData');
            $this->sendEndedRegistrationMail($userProfileData['email_s']);
            //$this->redirect('@homepage');
            $this->userRedirect('', $this->getUser()->getUID());

          }
        }

      }
    }

    if($this->getUser()->comeFromSignupOrMustBeCompleteData())
    {
      $this->setTemplate('profileNoSiteUser');
    }

  }

  /**
   * userRedirect()
   *
   * @params string $referer
   * @params string $UID
   */
  public function userRedirect($referer = '', $UID = '')
  {

    $defRedirect = $this->getUser()->getAttribute('gigyaRedirectUrl', '@homepage');


    if(str_replace("/index.php/","/",$defRedirect) == '/socialize/login') $this->redirect('@homepage');

    if($UID != '') 
    {
      if(str_replace("/index.php/","/",$defRedirect) == '/socialize/gamification') $this->redirect('@sfGigyaMyGamification?guid=' . $UID);
    }
   


    if($referer == '')
    {
      if($defRedirect == '@homepage' || strstr($defRedirect, '@sfGigyaMyGamification'))
      {
          $this->redirect($defRedirect);
      }
      else
      {
          $this->redirect('http://' . $this->getRequest()->getHost() . $defRedirect);
      }
    }
    else
    {
      $this->redirect($referer);
    }
  }

  /**
   * changePassword
   *
   * @params sfWebRequest $request
   *
   */
  public function executeChangePassword(sfWebRequest $request)
  {

    $this->form = new sfGigyaUserChangePasswordForm();

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $data['password_s'] = substr(md5($this->form->getValue('password_s')), 0, 32);

        $flag = $this->gigyaWrap->setUserProfileData($this->getUser()->getUID(),json_encode($data));
        $this->getUser()->setFlash('flagChangePassword', $flag);
      }
      else
      {
        $this->getUser()->setFlash('flagChangePassword', false);
      }
    }

    $this->redirect('@sfGigyaProfileUser');
  }

  public function executeSecure()
  {
    $this->getResponse()->setStatusCode(401);
    throw new sfSecurityException('401 Unauthorized');
  }

 
  public function executeNotifyAction(sfWebRequest $request)
  {
    $data_json=json_encode($this->gigyaWrap->notifyAction($this->getUser()->getUID(), $request->getParameter('action_id','empty')));
    return $this->renderText($data_json); 

  }
 
  /**
   * send change Profile data mail to new user
   * @param $sf_user
   */
  private function sendChangeProfileDataMail($sf_user, $email)
  {
    $mailer     = $this->getMailer();
    $fromMail   = sfConfig::get('app_mail_from_mail', 'noreply@foxtv.it');
    $fromName   = sfConfig::get('app_mail_from_name', 'FOX');
    $subject    = sfConfig::get('app_mail_subject_change_profile_data',"FOX - Riepilogo dati personali");
    $text       = $this->getPartial('mail_change_profile_data', array('sf_user' => $sf_user));
    $message    = $mailer->compose();
    $message->setSubject($subject);
    $message->setFrom(array($fromMail => $fromName));
    $message->setTo($email);
    $message->setBody($text, 'text/html');
    $mailer->send($message);
  }

  /**
   * sendEndedRegistrationMail
   *
   * @params email
   */
  private function sendEndedRegistrationMail($email)
  {
    $mailer     = $this->getMailer();
    $fromMail   = sfConfig::get('app_mail_from_mail', 'noreply@foxtv.it');
    $fromName   = sfConfig::get('app_mail_from_name', 'FOX');
    $subject    = sfConfig::get('app_mail_subject_registration_ended',"FOX - Registrazione completata");
    $text       = $this->getPartial('mail_registration_ended');
    $message    = $mailer->compose();
    $message->setSubject($subject);
    $message->setFrom(array($fromMail => $fromName));
    $message->setTo($email);
    $message->setBody($text, 'text/html');
    $mailer->send($message);


  }
}
