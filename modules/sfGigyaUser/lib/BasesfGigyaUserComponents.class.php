<?php

/**
 * Base components for the sfGigyaPlugin sfGigyaUser module.
 * 
 * @package     sfGigyaPlugin
 * @subpackage  sfGigyaUser
 * @author      Mauro DAlatri
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfGigyaUserComponents extends sfActions
{

  
  /**
   * execute login
   *
   */
  public function executeCmpLogin(sfWebRequest $request)
  {
    $gWrap        = new GigyaWrap();
    $this->form   = new sfGigyaUserLoginForm(null, Array('ref'=> $request->getReferer()));
    
    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $UID = $gWrap->userGCSLogin($this->form->getValue('email'), $this->form->getValue('password'));
        if($UID != -1)
        {
          if($sfGigyaUser = $this->getUser()->validateSignature($UID))
          {

            $this->getUser()->setGigyaSecurityUser($sfGigyaUser);

            if(!$this->getUser()->isConfirmed($UID)) $this->redirect('@sfGigyaLogin');

            
            $this->getUser()->setAttribute('gcsLogin',$UID);

            if($this->getUser()->isSiteUID()){
              $REFERER      = Misc::buildRefererForGigyaLoginRedirect($request);
              $this->getUser()->signin($sfGigyaUser);
              $gWrap->notifySiteLogin($UID);
              if($this->getUser()->getFirstName() != '' && $this->getUser()->getLastName()) $this->redirect($REFERER); 
              $this->getUser()->setAttribute('userMustBeCompleteData', true);
              $this->redirect('@sfGigyaProfileUser');
            }

            $this->redirect('@sfGigyaSignupUser');

          } 

        }
      }
    }

  }

  /**
   * execute cmpUserDectail
   *
   */
  public function executeCmpUserDectail()
  {

  }

  /**
   * execute cmpUserLogin
   *
   */
  public function executeCmpUserLogin()
  {

  }



}
