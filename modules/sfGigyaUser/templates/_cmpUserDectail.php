
<?php if($sf_user->getThumbnailURL()): ?>
  <img src='<?php echo $sf_user->getThumbnailURL();?>' width="50"><br />
<?php endif; ?>


<?php if(!$sf_user->isConfirmed($sf_user->getUID())): ?>

  <span class="notice">Ciao, devi comfermare il tuo account, <a href="<?php echo url_for('@sfGigyaConfirmRegistration?uid=' . $sf_user->getUID() )?>">Clicca qui</a></span><br /><br />

<?php else: ?>

  <?php if($sf_user->getNickname()): ?>

    <b><a href="<?php echo url_for('@sfGigyaProfileUser'); ?>"><?php echo $sf_user->getNickname(); ?></b></a>

  <?php elseif($sf_user->getLastName() || $sf_user->getFirstName()): ?>

    <a href="<?php echo url_for('@sfGigyaProfileUser'); ?>"><?php echo $sf_user->getFirstName();?> <?php echo $sf_user->getLastName();?></a>

  <?php else: ?>
    <span class="notice">Ciao, non hai ancora completato la tua registrazione, <a href="<?php echo url_for('@sfGigyaProfileUser')?>">Clicca qui</a></span><br /><br />

  <?php endif;?>

<?php endif;?>

| <a href="<?php echo url_for('@sfGigyaDeleteUser');?>">Unregister</a>
| <a href="<?php echo url_for('@sfGigyaLogout');?>">Logout</a> 




