<div id="loginDiv"></div>

<script type="text/javascript">  
  gigya.services.socialize.showLoginUI({
  containerID: "loginDiv"
  ,cid:''
  ,width:220
  ,height:60
  ,redirectURL: "<?php echo url_for('@sfGigyaVerify'); ?>"
  ,showTermsLink:false
  ,hideGigyaLink:true // remove 'Terms' and 'Gigya' links  
  ,UIConfig: "<config><body><controls><snbuttons buttonsize=\"25\"></snbuttons></controls></body></config>"
  ,requiredCapabilities: "login"
  ,useHTML: true
  ,pendingRegistration: true
  ,context: {
    context: "standalone"
  }
  ,lastLoginIndication: 'welcome'
  ,onLoad: eventHandler
  ,onClose: eventHandler
  ,onError: eventHandler
  ,onButtonClicked: eventHandler
});  
</script>

