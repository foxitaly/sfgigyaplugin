<?php $message = (empty($message) and $form->hasErrors()) ? 'DATI ERRATI' : $message ?>

<h4>Sei gi&agrave; registrato al sito?</h4>
<h5>Accedi inserendo username e password.</h5>

<?php if(!empty($message)) : ?>
    <div class="gigyaError"><h5><?php echo $message ?></h5></div>
<?php endif; ?>


<form method="post" action="<?php echo sf_gigya_url_login()?>">

    <div class="marginTop20 marginBottom20">
        <label>Username</label>
        <?php echo $form['email']->render(Array('id'=>'email','class' => errorClass($form['email']->hasError()))); ?>
        <label>Password</label>
        <?php echo $form['password']->render(Array('id'=>'password','class' => errorClass($form['password']->hasError()))); ?>
        <input type="submit" id="loginSubmit" value="Invia">
        <hr />
        <div class="size11">Non ricordi la password?&nbsp;<strong><a href="<?php echo url_for('@sfGigyaForgotPasswordUser'); ?>">Recupera password!</a></strong></div>
        <?php echo $form->renderHiddenFields(); ?>
        <div class="size11 marginTop10">Non sei registrato?&nbsp;<strong><a href="<?php echo sf_gigya_url_user_signup();?>">Registrati al sito</a></strong></div>
    </div>

</form>