<?php #include_component('sfGigyaUser','cmpLogin'); ?> 
<?php use_stylesheet('/sfGigyaPlugin/css/register.css')?>

<?php if($sf_user->hasFlash('err')): ?>

  <h1><?php echo $sf_user->getFlash('err');?></h1>

<?php endif; ?>

<div class="span-24">

  <div class="span-12">

    <h4>Sei gi&agrave; iscritto?</h4>


    <?php if($form->hasErrors()): ?>

      <h2 style="color:red; text-transform: uppercase; font-weight: bold">DATI ERRATI</h2>

    <?php endif; ?>

    
    <form method="post" action="<?php echo sf_gigya_url_login()?>">

      <p>
        <label>Username</label><br />
        <?php echo $form['email']->render(Array('id'=>'email','class' => errorClass($form['email']->hasError()))); ?>
      </p>
  
      <p>
        <label>Password</label><br />
        <?php echo $form['password']->render(Array('id'=>'password','class' => errorClass($form['password']->hasError()))); ?>
      </p>

      <p><input type="submit" value="Invia"></p>
      <a href="<?php echo url_for('@sfGigyaForgotPasswordUser')?>">Perso la password</a>

      <?php echo $form->renderHiddenFields(); ?>

    </form>

  </div>

  <div class="span-12 last">
    <h4>Loggati con uno dei tuoi profili sociali ;)</h4>
    <?php include_partial('socialLogin'); ?>
  </div>

</div>
