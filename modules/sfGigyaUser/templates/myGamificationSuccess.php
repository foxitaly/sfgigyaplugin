<script type='text/javascript'>
	//<![CDATA[ 
		$(window).load(function(){
			$(function(){
    			$('.infoBadge').mouseover(function(event){
        			event.preventDefault();
        			$(this).parent().find('.infoBadgeWindow').slideToggle();
    			});
				$('.infoBadge').mouseout(function(event){
        			event.preventDefault();
        			$(this).parent().find('.infoBadgeWindow').slideToggle();
    			});
			});
		});
	//]]>  
</script>
<div class="myGamificationHeader">

  <?php $overall  = gigyaWrap::getUserAchievement($myGamification->getRawValue(), "_default"); ?>
	<div class="userphoto"><img src="<?php echo $aUser['_thumbnailURL']; ?>" title="<?php echo $aUser['_firstName']; ?>" title="<?php echo $aUser['_firstName']; ?>" /></div>
    <div class="username"><h1 class="font1"><?php echo $aUser['_firstName']; ?></h1></div>
    <div class="userpoints"><span><?php echo $overall['pointsTotal']?></span> punti</div>
    <div class="userprofile">
      <?php if($sf_user->isAuthenticated() && $sf_user->isMyGUID($sf_request->getParameter('guid',null))): ?>
      <a href="<?php echo url_for('@sfGigyaProfileUser')?>" title="Modifica il tuo profilo" class="button">MODIFICA PROFILO &raquo;</a>
      <?php endif; ?>
    </div>
    <div class="clear"></div>
    <hr />
    <p><strong>Continua a dimostrare il tuo amore per le serie FOX!</strong><br />Guadagna altri punti visitando il nostro sito: ogni azione ti fa guadagnare punti e badge!!</p>
</div>
<ul class="myGamificationBody">
  <?php $foxLovers              = $challenges['_default']; ?>
  <?php $achievement   = gigyaWrap::getUserAchievement($myGamification->getRawValue(), $foxLovers['challengeID']); ?>

	<li class="generale">
    	<h2 class="left"><strong><?php echo $foxLovers['name'];?></strong> <span>Posizione: <?php echo $achievement['rank'];?></span><span>Punti: <?php echo $achievement['pointsTotal'];?></span></h2>
        <a href="<?php echo url_for('@sfGigyaOverallRank'); ?>" title="Via alla classifica" class="button right">VAI ALLA CLASSIFICA &raquo;</a>
        <div class="clear"></div>
        <hr />
    	<ul>

      <?php foreach($foxLovers['levels'] as $k => $badge): ?>
      <li class="<?php echo ($k==5) ? 'last' : ''?>">

          <?php $flag     = ($badge['points'] < $achievement['pointsTotal'])?>
          <?php $badgeURL = ($flag) ? $badge['badgeURL'] : $badge['lockedBadgeURL'];?>
        
          <img src="<?php echo $badgeURL; ?>" title="<?php echo $badge['title']?>" alt="<?php echo $badge['title']; ?> " />
          <h3><?php echo $badge['title']; ?></h3>
    
          <?php if(!$flag): ?>
          <a href="javascript:void();" title="Quanto di manca per conquistare questo badge?" class="infoBadge" id="toggler">?</a>
          <div class="infoBadgeWindow">
            <h4>Punti mancanti: <span><?php echo $badge['points'] - $achievement['pointsTotal'];?></span></h4>
            <div class="progressBackground"><div class="progressBar" style="width:<?php echo intval(100*$achievement['pointsTotal'] / $badge['points'])?>px"></div></div>
          </div>

          <?php endif; ?>
          
        
      </li>
      <?php endforeach; ?>
      <div class="clear"></div>
</ul>  
	</li>

    <?php $conta = 0; ?>
    <?php foreach($challenges as $aChallenge): ?>
    <?php if($aChallenge['challengeID'] == '_default') continue; ?>
    <?php $achievement   = gigyaWrap::getUserAchievement($myGamification->getRawValue(), $aChallenge['challengeID']); ?>

    <li class="prodotto <?php echo ($conta%2)  ? 'last' : 'left'?>">
    	<h2><strong><?php echo strtoupper($aChallenge['name']); ?></strong> <span>Posizione: <?php echo $achievement['rank'];  ?></span><span>Punti: <?php echo $achievement['pointsTotal'];?></span></h2>
        <hr />
        <a href="<?php echo url_for('@sfGigyaOverallRank?challengeName=' . $aChallenge['challengeID']);?>" title="Vai alla classifica" class="button left">VAI ALLA CLASSIFICA &raquo;</a>
        <?php
          $aShow = array_search($aChallenge['challengeID'], sfConfig::get('app_gigya_challenges',array()));
          if($aShow):
        ?>
        <a href="<?php echo url_for('@show?slug=' . $aShow);?>" title="Vai alla scheda show" class="button right">VAI ALLA SCHEDA SHOW &raquo;</a>
        <?php endif; ?>
        <div class="clear"></div>
        <hr />
        <ul class="challenge_<?php echo $aChallenge['challengeID'];?>">
            <?php foreach($aChallenge['levels'] as $k => $badge): ?>

            <?php $flag     = ($badge['points'] < $achievement['pointsTotal'])?>
            <?php $badgeURL = ($flag) ? $badge['badgeURL'] : $badge['lockedBadgeURL'];?>


            <li class="badge_challenge <?php echo ($k==3) ? 'last' : ''?>">
                <img src="<?php echo $badgeURL?>" title="<?php echo $badge['name']; ?>" alt="<?php echo $badge['title']; ?>" />
                <h3><?php echo $badge['title']; ?></h3>

                <?php if(!$flag): ?>

                <a href="javascript:void();" title="Quanto di manca per conquistare questo badge?" class="infoBadge" id="toggler">?</a>
                <div class="infoBadgeWindow">

                  <h4>Punti mancanti: <span><?php echo $badge['points'] - $achievement['pointsTotal'];?></span></h4>
                  <div class="progressBackground"><div class="progressBar" style="width:<?php echo intval(100*$achievement['pointsTotal'] / $badge['points'])?>px"></div></div>
                </div>

                <?php endif; ?>

            </li>
            <?php endforeach; ?>
            <div class="clear"></div>
		</ul>
    </li>
    <?php ++$conta; ?>
    <?php endforeach; ?>
    <?php $conta = 0; ?>
    <div class="clear"></div>
</ul>
