<h1>CLASSIFICA</h1>

<style>
  ol.gigyaOverallRank li { margin-bottom: 10px}

  table.fakeTable td { padding: 5px}

</style>


<ol class="gigyaOverallRank">
  <?php foreach($users as $user): ?>
  <?php $aUser = $user->getRawValue(); ?>
  <!--pre><?php print_r($user->getRawValue());?></pre-->
  <li>
    <table class="fakeTable">
    <tr>
      <td class="rank-number">
        <?php echo $aUser['rank']?>      
      </td>
      <td>
        <img src="<?php echo $aUser['photoURL']; ?>"> 
      </td>
      <td>
        <h2><?php echo $aUser['name'];?></h2>
        <ul class='gigyaChallengesRank'>

          <?php foreach( $aUser['achievements'] as $challenge): ?>

          <li>
            <strong>Challenge:</strong></strong><br />
            <strong>Punti Totali:</strong> <?php echo $challenge['pointsTotal']?><br />
            <strong>Punti ultimi 7 giorni:</strong> <?php echo $challenge['points7Days']?><br />
            <?php $aLevel =  ($challenge['level']) ? $challenge['levelTitle'] : "Beginner"; ?>
            <strong>Livello:</strong> <?php echo $aLevel;?>
          </li>

          <?php endforeach; ?>
        </ul>
      </td>
    </tr>
    </table>
  </li>
  <?php endforeach; ?>
</ol>

