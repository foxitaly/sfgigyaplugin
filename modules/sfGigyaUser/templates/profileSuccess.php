<?php use_stylesheet('/sfGigyaPlugin/css/register.css')?>

<?php if($sf_user->hasFlash('linkType')): ?>

  <h1><?php echo $sf_user->getFlash('linkType');?></h1>

<?php endif; ?>

<div class="section span-24">

    <div class="span-24 append-bottom">
      <h2>Cambia password</h2>

      <?php if($sf_user->hasFlash('flagChangePassword')): ?>
      
        <?php if($sf_user->getFlash('flagChangePassword')): ?>

        <div class="success">Modifiche avvenute correttamente</div>

        <?php else: ?>

        <div class="error">ATTENZIONE!!! Si sono verificati degli errori durante la modifica</div>

        <?php endif;?>  

      <?php endif; ?>

      <?php if($form->hasErrors()): ?>
        <div class="error">ATTENZIONE!!! Si sono verificati degli errori durante la modifica</div>
      <?php endif; ?>

      <form method="post" action="<?php echo sf_gigya_url_user_change_password() ?>">
        <div id="container">

        <p>
        <?php echo $formPassword['password_s']->renderLabel();?>
        <?php echo $formPassword['password_s']->render(Array('id'=>'password','class' => errorClass($formPassword['password_s']->hasError())));?>
        </p>
        <p>
        <?php echo $formPassword['repassword_s']->renderLabel();?>
        <?php echo $formPassword['repassword_s']->render(Array('id'=>'repassword','class' => errorClass($formPassword['repassword_s']->hasError())));?>
        </p>
        <input type='submit' value='Modifica'>
        <?php echo $formPassword->renderHiddenFields(); ?>
        </form>
    </div>
    <div class="span-24 append-bottom">
      <h2> Modifica i tuoi dati</h2>

      <?php if($sf_user->hasFlash('flagProfile')): ?>
      
        <?php if($sf_user->getFlash('flagProfile')): ?>

        <div class="success">Modifiche avvenute correttamente</div>

        <?php else: ?>

        <div class="error">ATTENZIONE!!! Si sono verificati degli errori durante la modifica</div>

        <?php endif;?>  

      <?php endif; ?>


      <form method="post" action="<?php echo sf_gigya_url_user_profile() ?>">

    <?php echo $form->renderGlobalErrors(); ?>
    <p>
    <?php echo $form['firstName']->renderLabel();?>
    <?php echo $form['firstName']->render(Array('id'=>'firstName','class' => errorClass($form['firstName']->hasError())));?>
    </p>
    <p>
    <?php echo $form['lastName']->renderLabel();?>
    <?php echo $form['lastName']->render(Array('id'=>'lastName', 'class' => errorClass($form['lastName']->hasError())))?>
    </p>
    <p>
    <?php echo $form['mobileNum_s']->renderLabel();?>
    <?php echo $form['mobileNum_s']->render(Array('id'=>'mobile', 'class' => errorClass($form['mobileNum_s']->hasError())))?>
    </p>
    <p>
    <?php echo $form['gender']->renderLabel();?>
    <?php echo $form['gender']->render(Array('id'=>'gender', 'class' => errorClass($form['gender']->hasError())))?>
    </p>
    <p>
    <?php echo $form['birthday']->renderLabel();?>
    <?php echo $form['birthday']->render(Array('id'=>'birthday', 'class' => errorClass($form['birthday']->hasError())))?>
    </p>
    <p>
    <?php echo $form['sky']->renderLabel();?>
    <?php echo $form['sky']->render(Array('id'=>'sky', 'class' => errorClass($form['sky']->hasError())))?>
    </p>
    <p>
    <?php echo $form['newsletter_s']->renderLabel();?>
    <?php echo $form['newsletter_s']->render(Array('id'=>'newsletter', 'class' => errorClass($form['newsletter_s']->hasError())))?>
    </p>
    <p>
    <?php echo $form['newsletter_channels']->renderLabel();?>
    <?php echo $form['newsletter_channels']->render(Array('id'=>'newsletter_channels', 'class' => errorClass($form['newsletter_channels']->hasError())))?>
    </p>
    <input type='submit' value='Modifica'>
    <?php echo $form->renderHiddenFields(); ?>
    </form>

    </div>

</div>




