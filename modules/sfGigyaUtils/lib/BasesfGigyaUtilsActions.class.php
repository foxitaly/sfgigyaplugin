<?php

/**
 * Base actions for the sfGigyaPlugin sfGigyaUtils module.
 * 
 * @package     sfGigyaPlugin
 * @subpackage  sfGigyaUtils
 * @author      Consulting Alpha
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfGigyaUtilsActions extends sfActions
{

  /**
   * preExecute
   *
   */
  public function preExecute()
  {
    $this->getResponse()->setContentType('application/json');
    $this->gigyaWrap = new GigyaWrap();
  }

  /**
   * postExecute
   *
   */
  public function postExecute()
  {
    $this->setLayout(false);
  }

  /**
   * execute getChallengeConfig
   *
   */
  public function executeGetChallengeConfig(sfWebRequest $request)
  {
    $challenges = $this->gigyaWrap->getChallenge();
    $this->gigyaResponse = json_encode($challenges);
    $this->setTemplate('json');
    
  }

  /**
   * execute getActionsPoint
   *
   */
  public function executeGetActionsPoint(sfWebRequest $request)
  {
    $points              = $this->gigyaWrap->getActionsPoint($request->getParameter('actionGigyaID','*'));
    $this->gigyaResponse = json_encode($points); 
    $this->setTemplate('json');
  }

  
}
