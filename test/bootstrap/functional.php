<?php
if (!isset($app))
{
  $app = 'frontend';
}

require_once dirname(__FILE__).'/../../../../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

function sfGigyaPlugin_cleanup()
{
  sfToolkit::clearDirectory(dirname(__FILE__).'/../fixtures/project/cache');
  sfToolkit::clearDirectory(dirname(__FILE__).'/../fixtures/project/log');
}
sfGigyaPlugin_cleanup();
register_shutdown_function('sfGigyaPlugin_cleanup');
$configuration = ProjectConfiguration::getApplicationConfiguration($app, 'test', isset($debug) ? $debug : true);
sfContext::createInstance($configuration);
