//------------------------------------------------------------------------------------------------------
// This JS file implements client side usage of Gigya (using Gigya's JS API).
// The methods that are implemented here, including Gigya's Login plugin, comprise part of Gigya's Social Login best practice implementation. 
// This file is used by login.php (Login form) and by register.php (Registration form).
//------------------------------------------------------------------------------------------------------

$(document).ready(function() {
	// Present Gigya's Login Plugin
	gigya.services.socialize.showLoginUI({ 
		containerID: 'loginPluginDiv' // The plugin is presented in the 'loginPluginDiv' <div> element (see definition in login.php and register.php)
		,height: 100
		,width: 313
		,showTermsLink: false
    ,hideGigyaLink: true
	  ,showWhatsThis: false
		,UIConfig: '<config><body><controls><snbuttons buttonsize="35" /></controls></body></config>'
		,buttonsStyle: 'fullLogo'
		,extraFields:'likes'
		,facebookExtraPermissions:'user_likes'
    ,redirectURL: "<?php echo url_for('@sfGigyaVerify'); ?>"
	});
	
});

//onLogout Event handler
function onLogoutHandler(eventObj) {
}

// onLogin Event handler
function onLoginHandler(eventObj) {	
}
