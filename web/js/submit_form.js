//------------------------------------------------------------------------------------------------------
// 	This file implements the forms' submission using AJAX
//------------------------------------------------------------------------------------------------------


function validateEmail(inputText){
	var atpos=inputText.indexOf("@");
	var dotpos=inputText.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=inputText.length)
		return false;
	else
		return true;
}

$(document).ready(function() {

	// Login Form submit
	//-------------------------------
	$("#login").click(function() {
		 if(!validateEmail( $("#loginEmail").val())){
			 $("#loginMessage").text("Please enter a valid e-mail address");
			 $('#loginMessage').show();
			 return false;
		}
		if ($("#loginPassword").val()==null || $("#loginPassword").val()=="" ){
			 $("#loginMessage").text("Please enter a password");
			 $('#loginMessage').show();
			 return false;
		}
		
		var formData = {
			loginEmail: $("#loginEmail").val(),
			loginPassword: $("#loginPassword").val(),
			type: 'login'
		};
		$('#loginMessage').hide();
		$.ajax({
			type: "POST",
			url: "post_handler.php",
			data: formData,
			dataType: "json",
			success: function(response){
				if(response.returnVal == "success"){
					window.location = "profile.php";
				}
				else{
					$("#loginMessage").text("Invalid Email and/or Password!");
					$('#loginMessage').show();
				}
			}
		});

		return false;
	});
	
	
	// Registration Form submit
	//-------------------------------
	$("#register").click(function() {
		if(!validateEmail( $("#email").val())){
			 $("#registrationMessage").text("Please enter a valid e-mail address");
			 $("#registrationMessage").show();
			return false;
		}
		if ($("#password").val()==null || $("#password").val()=="" || $("#repassword").val()==null || $("#repassword").val()==""){
			 $("#registrationMessage").text("Please enter a password");
			 $("#registrationMessage").show();
			 return false;
		}
		if ($("#password").val() !=  $("#repassword").val()){
			 $("#registrationMessage").text("The Password and verified password do not match!");
			 $("#registrationMessage").show();
			 return false;
		}			
		var formData = {
			email: $("#email").val(),
			password: $("#password").val(),
			type: 'register'
		};
		$("#registrationMessage").text("");
		$("#registrationMessage").hide();
		$.ajax({
			type: "POST",
			url: "post_handler.php",
			data: formData,
			dataType: "json",
			success: function(response)
			{
				switch(response.returnVal)
				{
					case "success": //success
						window.location = "update_user_profile.php";
						break;
					case "missingParameter":  // Email or password is missing
						$("#registrationMessage").text("Regsitration Failed. Please provide email and password");
						 $("#registrationMessage").show();
						break;
					case "emailExist": // The email provided by the user already exists in the DB
						$("#registrationMessage").text("Regsitration Failed. The email you have provided already exists in our system.");
						$("#registrationMessage").show();
						break;
					default:
						$("#registrationMessage").text("Regsitration Failed. Error message: " + response.returnVal);
					 	$("#registrationMessage").show();	
				}	
			}
		});
		return false;
	});
	
	// Link Accounts Form handler
	//-------------------------------
	$("#linkAccount").click(function() {
		if(!validateEmail( $("#linkEmail").val())){
			 $("#linkAccountMessage").text("Error: Email is missing");
			 $("#linkAccountMessage").show();
			return false;
		}
		if ($("#loginPassword").val()==null || $("#loginPassword").val()=="" ){
			 $("#linkAccountMessage").text("Please enter a password");
			 $("#linkAccountMessage").show();
			 return false;
		}
		var formData = {
			loginEmail: $("#linkEmail").val(),
			loginPassword: $("#loginPassword").val(),
			type: 'linkAccount'
		};
		
		$("#linkAccountMessage").text("");
		$("#linkAccountMessage").hide();
		$.ajax({
			type: "POST",
			url: "post_handler.php",
			data: formData,
			dataType: "json",
			success: function(response){
				if(response.returnVal == "success")
					window.location = "profile.php";
				else{
					$("#linkAccountMessage").text("Wrong Password!");
					$("#linkAccountMessage").show();
				}
			}
		});

		return false;
	});
	
	//Password Reminder - send email reminder.
	//-------------------------------
	$("#forgotPassword").click(function() {
		if(!validateEmail( $("#forgotEmail").val())){
			 $("#forgotMessage").text("Please enter a valid e-mail address");
			 $("#forgotMessage").show();
			return false;
		}
		var formData = {
			forgotEmail: $("#forgotEmail").val(),
			type: 'forgotPass'
		};

		$("#forgotMessage").text("");
		$("#forgotMessage").hide();
		$.ajax({
			type: "POST",
			url: "post_handler.php",
			data: formData,
			dataType: "json",
			success: function(response){
				switch(response.returnVal)
				{
					case "success": //success
						//window.location = "update_user_profile.php";
						 $("#SuccessEmailReminder").show();
						 $("#page").hide();
						break;
					case "emailNotExist": // The email provided by the user doesn't exists in the DB
						$("#forgotMessage").text("Sorry, we can't find such email address in our records");
						$("#forgotMessage").show();
						break;
					case "EmailFail": // The email provided by the user doesn't exists in the DB
						$("#forgotMessage").text("Error while sending the email, please try again.");
						$("#forgotMessage").show();
						break;	
					default:
						$("#forgotMessage").text("Sending email failed. Error message: " + response.returnVal);
						$("#forgotMessage").show();	
				}	
			}
		});
		return false;
	});
	
	// User provided missing Email
	//-------------------------------
	$("#provideEmail").click(function() {
		if(!validateEmail( $("#loginEmail").val())){
			 $("#provideEmailMessage").text("Please enter a valid e-mail address");
			 $("#provideEmailMessage").show();
			return false;
		}
		var formData = {
			loginEmail: $("#loginEmail").val(),
			type: 'snEmail'
		};
		
		$("#provideEmailMessage").text("");
		$("#provideEmailMessage").hide();
		$.ajax({
			type: "POST",
			url: "post_handler.php",
			data: formData,
			dataType: "json",
			success: function(response){
				switch(response.returnVal)
				{
					case "success": //success
						window.location = "update_user_profile.php";
						break;
					case "emailExist": // The email provided by the user already exists in the DB
						$("#provideEmailMessage").text("The email you have provided already exists in our system. Please use a different email.");
						$("#provideEmailMessage").show();
						break;
					default:
						$("#provideEmailMessage").text("Regsitration Failed. Error message: " + response.returnVal);
					 	$("#provideEmailMessage").show();	
				}	
			}
		});

		return false;
	});
	
	// Update User Data Form handler
	//-------------------------------
	$("#updateUserData").click(function() {
		//validate form
		if(!validateEmail( $("#email").val())){
			 $("#updateUserDataMessage").text("Please enter a valid e-mail address");
			 $('#updateUserDataMessage').show();
			return false;
		}
		if ($("#firstname").val()==null || $("#firstname").val()==""){
			 $("#updateUserDataMessage").text("First name must be filled");
			 $('#updateUserDataMessage').show();
			 return false;
		}
		if ($("#lastname").val()==null || $("#lastname").val()==""){
			 $("#updateUserDataMessage").text("Surname must be filled");
			 $('#updateUserDataMessage').show();
			 return false;
		}
		if ($("#postcode").val() ==  ""){
			 $("#updateUserDataMessage").text("Please enter postcode");
			 $('#updateUserDataMessage').show();
			 return false;
		}
		
		if ($("#country").val() ==  ""){
			 $("#updateUserDataMessage").text("Please choose a country");
			 $('#updateUserDataMessage').show();
			 return false;
		}
		if ($("#country").val() ==  ""){
			 $("#updateUserDataMessage").text("Please choose a country");
			 $('#updateUserDataMessage').show();
			 return false;
		}
		if ($("#yearbirth").val() ==  ""){
			 $("#updateUserDataMessage").text("Please choose your birth year");
			 $('#updateUserDataMessage').show();
			 return false;
		}	
		if (!$('#termagree').attr('checked')) {
			 $("#updateUserDataMessage").text("Please agree to the terms and condition");
			 $('#updateUserDataMessage').show();
			 return false;
		}
		
		var tempGender = "f";
		if($('input:radio[name=gender]')[1].checked == true)
			tempGender = "m";
		var formData = {
			firstname: $("#firstname").val(),
			lastname: $("#lastname").val(),
			email: $("#email").val(),
			postcode: $("#postcode").val(),
			country: $("#country").val(),
			mobileNum: $("#mobileNum").val(),
			gender: tempGender,
			yearbirth: $("#yearbirth").val(),
			newsletter: $('#newsletter').attr('checked'),
			type: 'updateProfileData'
		};
		
		$("#updateUserDataMessage").text("");
		$('#updateUserDataMessage').hide();
		$.ajax({
			type: "POST",
			url: "post_handler.php",
			data: formData,
			dataType: "json",
			success: function(response)
			{
				switch(response.returnVal)
				{
					case "success": //success
						window.location = "profile.php";
						break;
					case "notLoggedIn": // Login session is not available
						$("#updateUserDataMessage").text("Regsitration Failed. Please return to the registration page and try again");
						$('#updateUserDataMessage').show();
						break;
					default:
						$("#updateUserDataMessage").text("Failed setting profile data. Error message: " + response.returnVal);
						$('#updateUserDataMessage').show();
							
				}	
			}
		});
		return false;
	});
});