var newspage=null;

(function(jQuery){
	//## SLIDE ##//
	jQuery.fn.slide=function(opt){
		/*
		* btnleft = object ปุ่มเลื่อนซ้าย
		* btnright = object ปุ่มเลื่อนขวา
		* bullet = เปิดใช้ bullet true หรือ false
		* bulletdisplay = obj กล่องที่ใช้วาง bullet
		* bulletclassblur = class ที่ใช้แสดง bullet blur
		* bulletclassfocus = class ที่ใช้แสดง bullet focus
		* bulletmargin = ระยะห่างแต่ละ bullet
		* delay = เวลาวินาทีในการ slide
		* displayheight = height ของพื้นที่ ที่ใช้แสดง
		* displaywidth = width ของพื้นที่ ที่ใช้แสดง
		* innercontainer = obj ที่ใช้เก็บ content ข้างใน
		* innerwidth = width ของข้อมูลข้างใน *ระบุหน่วย width มาด้วย
		* startpage = page เริ่มต้น
		* stepwidth = width ที่จะให้เลื่อนแต่ละครั้ง *ระบุหน่วย width มาด้วย
		* evt = event ในการ action เช่น click, mouseenter, mouseover
		*/
		
		var params={
			bullet:false,
			delay:1,
			displaywidth:0,
			easing:'swing',
			evt:'click',
			startpage:1,
			stepwidth:0
		};
		
		//initial for this
		var opt=jQuery.extend(params,opt);
		delay=opt.delay*1000;
		(opt.displayheight==0)?displayheight=jQuery(this).height()+'px':displayheight=opt.displayheight;
		(opt.displaywidth==0)?displaywidth=jQuery(this).width()+'px':displaywidth=opt.displaywidth;
		(opt.stepwidth==0)?stepwidth=jQuery(this).width()+'px':stepwidth=opt.stepwidth;
		totalPage=Math.ceil(parseFloat(opt.innerwidth)/parseFloat(opt.stepwidth))-Math.ceil(parseFloat(displaywidth)/parseFloat(stepwidth));
		newspage=opt.startpage-1;
		
		return this.each(function(){
			obj=jQuery(this);
			obj.css({
				'overflow':'hidden',
				'position':'relative',
				'width':(opt.displaywidth==0)?obj.width()+'px':opt.displaywidth,
				'height':(opt.displayheight==0)?obj.height()+'px':opt.displayheight
			});
			jQuery(opt.innercontainer).css({
				'width':opt.innerwidth,
				'position':'relative',
				'left':(0-((opt.startpage-1)*parseFloat(stepwidth)))+'px'
			});
			jQuery(opt.btnleft).attr('href','javascript:').bind(opt.evt,function(){
				newspage=(newspage*1)-1;
				if(newspage<0){
					newspage=0;
				}else{
					jQuery(opt.innercontainer).animate({
						'left':(0-(newspage*(parseFloat(stepwidth))))+'px'
					},delay);
				}
				if(opt.bullet)bullet();
			});
			jQuery(opt.btnright).attr('href','javascript:').bind(opt.evt,function(){
				newspage=(newspage*1)+1;
				if(newspage>totalPage){
					newspage=totalPage;
				}else{
					jQuery(opt.innercontainer).animate({
						'left':(0-(newspage*(parseFloat(stepwidth))))+'px'
					},delay);
				}
				if(opt.bullet)bullet();
			});
			if(opt.bullet)bullet();
			
			function bullet(){
				if(opt.bullet){
					objBullet=jQuery(opt.bulletdisplay);
					objBullet.css({
						'overflow':'hidden',
						'width':(parseFloat(opt.bulletmargin)*(totalPage+1))+'px'
					}).html('');
					for(i=0;i<=totalPage;i++){
						if(i==newspage){
							objBullet.append('<div class="'+opt.bulletclassfocus+'" id="__news__'+i+'"></div>');
						}else{
							objBullet.append('<div class="'+opt.bulletclassblur+'" id="__news__'+i+'" style="cursor:pointer;"></div>');
							jQuery('#__news__'+i+'').click(function(){
								tmp=jQuery(this).attr('id');
								tmp=tmp.split('__news__');
								newspage=tmp[1]*1;
								bullet();
								if(newspage>totalPage){
									newspage=totalPage;
								}else{
									jQuery(opt.innercontainer).animate({
										'left':(0-(newspage*(parseFloat(stepwidth))))+'px'
									},delay);
								}
							});
						}
					}
				}
			}
		});
	}
})(jQuery);